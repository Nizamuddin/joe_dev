<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\widgets\ActiveForm;
use app\models\LoginForm;
use app\models\User;
use app\models\BiodataUser;
use app\models\Siswa;
use app\models\ContactForm;
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                //     'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $modelUser = new User();
        $modelLogin = new LoginForm();
        $modelSiswa = new Siswa();
        $modelBioData = new BiodataUser();
        if ($modelLogin->load(Yii::$app->request->post()) && $modelLogin->login()) {
            return $this->goBack();
        }else{
            return $this->render('login', [
                'modelLogin' => $modelLogin,
                'modelUser'=>$modelUser,
                'modelSiswa'=>$modelSiswa,
                'modelBioData'=>$modelBioData
            ]);
        }
    }

    public function actionRegist()
    {
        
        $modelSiswa = new Siswa();
        $modelUser = new User();
        $modelBioData = new BiodataUser();

        if (Yii::$app->request->isAjax && $modelUser->load(Yii::$app->request->post())) {
                $nm= $_POST['User']['email'];
                $result = User::find()->select(['email'])->where(['email' => "$nm"])->one();
                if ($result) {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return \yii\widgets\ActiveForm::validate($modelUser, 'email');
                } else {
                    return false;
            }
        }
        $biodataUser = Yii::$app->request->post('BiodataUser');
        $siswa = Yii::$app->request->post('Siswa');
        $user = Yii::$app->request->post('User');

        $modelUser->email = $user['email'];
        $modelUser->password = Yii::$app->security->generatePasswordHash($user['password']);
        $modelUser->status = 1;
        $modelUser->type = 2;

        if (Yii::$app->request->isAjax && $modelBioData->load(Yii::$app->request->post())) {
                $nm= $_POST['BiodataUser']['no_ktp'];
                $result = BiodataUser::find()->select(['no_ktp'])->where(['no_ktp' => "$nm"])->one();
                if ($result) {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return \yii\widgets\ActiveForm::validate($modelBioData, 'no_ktp');
                } else {
                    return false;
            }
        }

        


        if ($modelUser->save()) {
            $timeFile = strtotime("now");
            $modelSiswa->id_user = $modelUser->id_user;
            $modelSiswa->pendidikan_terakhir = $siswa['pendidikan_terakhir'];
            $modelSiswa->jurusan = $siswa['jurusan'];
            $modelSiswa->status = 'mendaftar';
            $fotoKtp = UploadedFile::getInstanceByName('Siswa[fotoKtp]');
            $pathFotoKtp = Siswa::PATH_.$modelUser->id_user;
            FileHelper::createDirectory($pathFotoKtp, $mode = 0775, $recursive = true);
            $namaFileFotoKtp = $modelUser->id_user.'-'.$timeFile.'-KTP'; 
            $fotoKtp->saveAs($pathFotoKtp ."/". $namaFileFotoKtp . '.' . $fotoKtp->extension);
            $modelSiswa->foto_ktp = $namaFileFotoKtp;

            $modelSiswa->save();

            $modelBioData->id_user = $modelUser->id_user;
            $modelBioData->nama = $biodataUser['nama'];
            $modelBioData->no_ktp = $biodataUser['no_ktp'];
            $modelBioData->tempat_lahir = $biodataUser['tempat_lahir'];
            $modelBioData->tgl_lahir = $biodataUser['tgl_lahir'];
            $modelBioData->jenis_kelamin = $biodataUser['jenis_kelamin'];
            $modelBioData->alamat = $biodataUser['alamat'];
            $modelBioData->hp = $biodataUser['hp'];

            $timeFile = strtotime("now");
            $foto = UploadedFile::getInstanceByName('BiodataUser[file]');
            $path = Siswa::PATH_.$modelUser->id_user;
            
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $namaFile = $modelUser->id_user.'-'.$timeFile.'-Foto'; 
            
            $foto->saveAs($path ."/". $namaFile . '.' . $foto->extension);
            $modelBioData->foto = $path."/".$namaFile . '.' . $foto->extension;
            
            if ($modelBioData->save()) {
                \Yii::$app->getSession()->setFlash('success', "<i class='fa fa-check-circle'></i> Pendaftaran Telah Berhasil");
                return $this->redirect(['login']);
            }
        }else{
            $this->layout = 'main-login';
            $modelLogin = new LoginForm();
            return $this->render('login', [
                'modelLogin' => $modelLogin,
                'modelUser'=>$modelUser,
                'modelSiswa'=>$modelSiswa,
                'modelBioData'=>$modelBioData
            ]);
        }
    }

    public function actionUsernameValidation()
    {
        $model = new User();
        $modelBioData = new BiodataUser();
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) ){
            Yii::$app->response->format ='json';
            return ActiveForm::validate($model);
        }
    }
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
