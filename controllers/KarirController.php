<?php

namespace app\controllers;

use Yii;
use app\models\Karir;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KarirController implements the CRUD actions for Karir model.
 */
class KarirController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Karir models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataKarir = Karir::find()->where(['id_user'=>User::getIdUser()])->all();

        return $this->render('index', [
            'dataKarir' => $dataKarir,
        ]);
    }

    /**
     * Displays a single Karir model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Karir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Karir();

        if ($model->load(Yii::$app->request->post())) {
            $modelKarir = Yii::$app->request->post('Karir')['karir'];
            foreach ($modelKarir as $key => $value) {
                $newModel = new Karir();
                $newModel->id_user = User::getIdUser();
                $newModel->instansi = ($value['instansi']==null ? null : $value['instansi']);
                $newModel->posisi = ($value['posisi']==null ? null : $value['posisi']);
                $newModel->tahun_masuk = ($value['tahun_awal']==null ? null : $value['tahun_awal']);
                $newModel->tahun_keluar = ($value['tahun_akhir']==null ? null : $value['tahun_akhir']);
                $newModel->type ='1';
                $newModel->save(false);
            }
            return $this->redirect(['index']);
        }else{
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
            
        }

    }

    /**
     * Updates an existing Karir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Karir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Karir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Karir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Karir::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
