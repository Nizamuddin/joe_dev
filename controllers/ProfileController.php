<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use app\models\Kursus;
use app\models\Coach;
use app\models\AbsensiCoach;
use app\models\AbsensiSiswa;
use app\models\Jadwal;
use app\models\SiswaKursus;
use app\models\Karir;
use app\models\BiodataUser;
use app\models\User;
use app\models\Siswa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
/**
 * KarirController implements the CRUD actions for Karir model.
 */
class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Karir models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelBiodataUser = BiodataUser::find()->where(['id_user'=>User::getIdUser()])->one();
        
        if (User::getType()==3) {
            $model = Coach::findOne(User::getIdCoach());
        }elseif(User::getType()==2){
            $model = Siswa::findOne(User::getIdSiswa());
        }
        
            $modelUser = User::findOne(User::getIdUser());

        return $this->render('update', [
            'modelBioData'  => $modelBiodataUser,
            'modelUser' => $modelUser,
            'model'         => $model
        ]);
    }

    public function actionUpdateBiodate()
    {
        $modelBiodataUser = BiodataUser::find()->where(['id_user'=>User::getIdUser()])->one();
        
        if (User::getType()==3) {
            $model = Coach::findOne(User::getIdCoach());
        }elseif(User::getType()==2){
            $model = Siswa::findOne(User::getIdSiswa());
        }
        
        $modelUser = User::findOne(User::getIdUser());

        $model->load(Yii::$app->request->post());
        $modelUser->load(Yii::$app->request->post());
        $modelBiodataUser->load(Yii::$app->request->post());


        $file = UploadedFile::getInstance($modelBiodataUser, 'file');
        if ($file) {
            $timeFile = strtotime("now");
            
            unlink($modelBiodataUser->foto);
            
            if (User::getType()==3)
                $path = Coach::PATH_.$modelUser->id_user;
            elseif(User::getType()==2)
                $path = Siswa::PATH_.$modelUser->id_user;


            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $namaFile = $modelUser->id_user.'-'.$timeFile; 
            echo $file->saveAs($path ."/". $namaFile . '.' . $file->extension);
            $modelBiodataUser->foto = $path."/".$namaFile . '.' . $file->extension;
        }
        $model->save();
        $modelUser->save();
        $modelBiodataUser->save();
        return $this->redirect(['index']);
    }

    public function actionUpdateAccount()
    {
        $modelUser = User::findOne(User::getIdUser());
        $modelUser->load(Yii::$app->request->post());

        if ($modelUser->newPassword) {
            $modelUser['password'] =Yii::$app->security->generatePasswordHash($modelUser['newPassword']);
        }
        $modelUser->save();
        return $this->redirect(['index']);
    }
    /**
     * Displays a single Karir model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Karir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Karir();

        if ($model->load(Yii::$app->request->post())) {
            $modelKarir = Yii::$app->request->post('Karir')['karir'];
            foreach ($modelKarir as $key => $value) {
                $newModel = new Karir();
                $newModel->id_user = User::getIdUser();
                $newModel->instansi = ($value['instansi']==null ? null : $value['instansi']);
                $newModel->posisi = ($value['posisi']==null ? null : $value['posisi']);
                $newModel->tahun_masuk = ($value['tahun_awal']==null ? null : $value['tahun_awal']);
                $newModel->tahun_keluar = ($value['tahun_akhir']==null ? null : $value['tahun_akhir']);
                $newModel->type ='1';
                $newModel->save(false);
            }
            return $this->redirect(['index']);
        }else{
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
            
        }

    }

    /**
     * Updates an existing Karir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Karir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Karir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Karir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Karir::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
