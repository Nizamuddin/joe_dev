<?php

namespace app\controllers;

use Yii;
use app\models\Pembayaran;
use app\models\Siswa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * PembayaranController implements the CRUD actions for Pembayaran model.
 */
class PembayaranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pembayaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataBelum = Pembayaran::find()->where(['status'=>0])->all();
        $dataSudah = Pembayaran::find()->where(['status'=>1])->all();

        return $this->render('index', [
            'dataBelum' => $dataBelum,
            'dataSudah' => $dataSudah,
        ]);
    }

    /**
     * Displays a single Pembayaran model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pembayaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pembayaran();

        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'file');
            $timeFile = strtotime("now");
            $path = Siswa::PATH_.$model->getIdUser();
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $namaFile = $model->getIdUser().'-'.$timeFile; 
            $file->saveAs($path ."/". $namaFile . '.' . $file->extension);
            $model->bukti_transfer = $path ."/". $namaFile . '.' . $file->extension;
            $model->status = 0;
            $model->id_siswa = $model->getIdSiswa();
            $model->save(false);

            $modelSiswa = Siswa::findOne($model->getIdSiswa());
            $modelSiswa->status='membayar';
            $modelSiswa->save(false);

            \Yii::$app->getSession()->setFlash('success', 'Saved');
            return $this->redirect(['create']);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
            
        }

    }

    public function actionVerifikasi($id)
    {
        $model = $this->findModel($id);
        $model->status =1;
        $model->save(false);
        $modelSiswa = Siswa::findOne($model->id_siswa);
        $modelSiswa->status = 'peserta';
        $modelSiswa->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Saved');
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Pembayaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pembayaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pembayaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pembayaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pembayaran::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
