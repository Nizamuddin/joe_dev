<?php

namespace app\controllers;

use Yii;
use app\models\Kursus;
use app\models\AbsensiCoach;
use app\models\AbsensiSiswa;
use app\models\Jadwal;
use app\models\User;
use app\models\Siswa;
use app\models\SiswaKursus;
use app\models\Search\Kursus as KursusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
/**
 * KursusController implements the CRUD actions for Kursus model.
 */
class KursusController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kursus models.
     * @return mixed
     */
    public function actionIndex()
    {

        if (User::getType()==2 ) {
            if (User::getStatusSiswa()=='peserta' || User::getStatusSiswa()=='memilih kursus') {
                $arrayIdKursus = SiswaKursus::find()->select('id_kursus')->where(['id_siswa'=>User::getIdSiswa()])->distinct()->asArray()->all();
                $arrTemp = array();
                foreach ($arrayIdKursus as $key => $value) {
                    $arrTemp[$key]=$value['id_kursus'];
                }
                $query = Kursus::find()->where(['in', 'id', $arrTemp]);
            }else{
                $query = Kursus::find()->where(['status'=>1 ]);
            }
        }else if (User::getType()==3) {
            $arrJadwal = Jadwal::find()->select('id_kursus')->where(['id_coach'=>User::getIdCoach()])->distinct()->asArray()->all();
            $arrTemp = array();
            foreach ($arrJadwal as $key => $value) {
                $arrTemp[$key]=$value['id_kursus'];
            }
            $query = Kursus::find()->where(['in', 'id', $arrTemp]);
        }else{
            $query = Kursus::find();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataKursus' => $dataProvider->getModels(),
        ]);
    }

    /**
     * Displays a single Kursus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $modelJadwal = Jadwal::find()->where(['id_kursus'=>$id]);

        if (User::getType()==3) {
            $modelJadwal = Jadwal::find()->where(['id_kursus'=>$id])->andWhere(['id_coach'=>User::getIdCoach()]);
        }
        if (User::getType() == 1 || User::getType() == 3) {
            return $this->render('view', [
                'model' => $this->findModel($id),
                'modelJadwal'=>$modelJadwal->all()
            ]);
        }elseif(User::getType() == 4){
            return $this->render('view', [
                'model' => $this->findModel($id),
                'modelJadwal'=>$modelJadwal->all(),
                'dataSiswaKursus'=>SiswaKursus::find()->where(['id_kursus'=>$id])->all()
            ]);
        }else{
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
                'modelJadwal'=>$modelJadwal->all()
            ]);
        }
    }

    public function actionUpdateJadwal($id)
    {   
        $modelJadwal = Jadwal::findOne($id);

        if ($modelJadwal->load(Yii::$app->request->post())) {
            $modelJadwal->waktu_mulai = date("H:i", strtotime($modelJadwal->waktu_mulai));
            $modelJadwal->waktu_selesai = date("H:i", strtotime($modelJadwal->waktu_selesai));
            $modelJadwal->save();
            return $this->redirect(['view', 'id'=>$modelJadwal->id_kursus]);
        }else{
            $modelJadwal->waktu_mulai = date('h:i:s a m/d/Y', strtotime($modelJadwal->waktu_mulai));;
            $modelJadwal->waktu_selesai = date('h:i:s a m/d/Y', strtotime($modelJadwal->waktu_selesai));;
            return $this->renderAjax('_form_jadwal', [
                'modelJadwal'=>$modelJadwal
            ]);
            
        }
    }

    public function actionPilihKursus($idKursus)
    {
        $model = new SiswaKursus();
        $model->id_siswa = User::getIdSiswa();
        $model->id_kursus = $idKursus;
        $model->tgl_pilih = date("Y-m-d");
        $model->save();
        $modelSiswa = Siswa::findOne(User::getIdSiswa());
        $modelSiswa->status = 'memilih kursus';
        $modelSiswa->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Tersimpan');
        return $this->redirect(['index']);
    }

    /**
     * Creates a new Kursus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kursus();
        $modelJadwal = new Jadwal();
        // print_r($model->arrayMapel);
        if ($model->load(Yii::$app->request->post()) ) {
            $modelJadwal->load(Yii::$app->request->post());
            $countJadwal =count(Yii::$app->request->post('coach'));
            
            $model->tanggal_awal = $modelJadwal['hari'][0];
            $model->tanggal_akhir = $modelJadwal['hari'][$countJadwal-1];
            $model->saving();
            for ($i=0; $i < $countJadwal; $i++) { 
                $newModelJadwal = new Jadwal();
                $newModelJadwal->id_kursus = $model->id;
                $newModelJadwal->id_coach = Yii::$app->request->post()['coach'][$i];
                $newModelJadwal->waktu_mulai = $modelJadwal['waktu_mulai'][$i];
                $newModelJadwal->waktu_selesai = $modelJadwal['waktu_selesai'][$i];
                $newModelJadwal->hari = $modelJadwal['hari'][$i];
                $newModelJadwal->mapel = $modelJadwal['mapel'][$i];
                $newModelJadwal->id_ruangan = Yii::$app->request->post()['ruangan'][$i];
                $newModelJadwal->save();
            }
            \Yii::$app->getSession()->setFlash('success', 'Saved');
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'modelJadwal'=>$modelJadwal
        ]);
    }


    public function actionViewAbsensi($idJadwal)
    {
        $dataAbsensi = AbsensiSiswa::find()->where(['id_jadwal'=>$idJadwal])->all();
        return $this->renderAjax('view_absensi',[
            'dataAbsensi'=>$dataAbsensi,
        ]);
    }

    public function actionViewSiswa($idKursus, $idJadwal)
    {
        $dataSiswa = SiswaKursus::find()->where(['id_kursus'=>$idKursus])->all();

        return $this->renderAjax('view_siswa',[
            'dataSiswa'=>$dataSiswa,
            'idJadwal'=>$idJadwal,
            'idKursus'=>$idKursus
        ]);
    }

    public function actionSaveAbsensi()
    {
        $idSiswa = Yii::$app->request->post('absensi');
        $idJadwal = Yii::$app->request->post('id_jadwal');
        $idKursus = Yii::$app->request->post('id_kursus');
        $modelAbsensiCoach = new AbsensiCoach();
        $modelAbsensiCoach->id_coach = User::getIdCoach();
        $modelAbsensiCoach->keterangan = 'h';
        $modelAbsensiCoach->id_jadwal = $idJadwal;
        $modelAbsensiCoach->save();

        $dataSiswa = SiswaKursus::find()->where(['id_kursus'=>$idKursus])->all();

        foreach ($dataSiswa as $key => $value) {
            $modelAbsensiSiswa = new AbsensiSiswa();
            $modelAbsensiSiswa->id_siswa = $value->id_siswa;
            $modelAbsensiSiswa->keterangan = in_array($value->id_siswa, $idSiswa) ? 'h':'i'; 
            $modelAbsensiSiswa->id_jadwal = $idJadwal;
            echo in_array($value->id_siswa, $idSiswa) ? 'h':'i';
            $modelAbsensiSiswa->save();
        }
        return $this->redirect(['view', 'id'=>$idKursus]);
    }

    public function actionSetujuiKursus($idKursus)
    {
        $modelKursus = $this->findModel($idKursus);
        $modelKursus->status = 1; 
        $modelKursus->save();
        \Yii::$app->getSession()->setFlash('success', 'Data Tersimpan');
        return $this->redirect(['view', 'id'=>$idKursus]);
    }
    /**
     * Updates an existing Kursus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Kursus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kursus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kursus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kursus::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
