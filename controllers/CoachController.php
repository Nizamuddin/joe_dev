<?php

namespace app\controllers;

use Yii;
use app\models\Coach;
use app\models\Karir;
use app\models\BiodataUser;
use app\models\OrganisasiCoach;
use app\models\search\CoachSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;
/**
 * CoachController implements the CRUD actions for Coach model.
 */
class CoachController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Coach models.
     * @return mixed
     */
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Coach::find(),
        ]);

        return $this->render('index', [
            'dataCoach' => $dataProvider->getModels(),
        ]);
    }

    public function actionVerifikasi($id)
    {
        $model = Coach::findOne($id);
        $model->status = 1;
        $model->save(false);
        \Yii::$app->getSession()->setFlash('success', "<i class='fa fa-check-circle'></i> Pendaftaran Telah Berhasil");
        return $this->redirect(['index']);
    }

    /**
     * Displays a single Coach model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $dataCoach = Coach::findOne($id);
        $pengalamanKerja = Karir::find()
                            ->where(['id_user'=>$dataCoach->user->id])
                            ->andWhere(['type'=>2])
                            ->all();
        $organisasi = OrganisasiCoach::find()
                        ->where(['id_coach'=>$id])
                        ->all();
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
            'dataPengalamanKerja'=> $pengalamanKerja,
            'dataOrganisasi'=>$organisasi
        ]);
    }

    /**
     * Creates a new Coach model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Coach();
        $modelUser =  new User();
        $modelBiodataUser =  new BiodataUser();
        $modelOrganisasiCoach = new OrganisasiCoach();
        $modelKarir = new Karir();
        if ($modelUser->load(Yii::$app->request->post())) {
            $model->load(Yii::$app->request->post());
            $modelBiodataUser->load(Yii::$app->request->post());
            $organisasiCoach = Yii::$app->request->post('OrganisasiCoach')['organisasi'];
            $karirCoach = Yii::$app->request->post('Karir')['karir'];

            $modelUser->password = Yii::$app->getSecurity()->generatePasswordHash('initial');
            $modelUser->status = 1;
            $modelUser->type = 3;

            if ($modelUser->save()) {

                $model->id_user = $modelUser->id;
                $model->status = 0;
                $model->save();

                $modelBiodataUser->id_user = $modelUser->id;
                $timeFile = strtotime("now");
                $foto = UploadedFile::getInstances($modelBiodataUser, 'file')[0];
                $path = Coach::PATH_.$modelUser->id_user;
                FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
                $namaFile = $model->id_user.'-'.$timeFile; 
                $foto->saveAs($path ."/". $namaFile . '.' . $foto->extension);
                $modelBiodataUser->foto = $path."/".$namaFile . '.' . $foto->extension;
                $modelBiodataUser->save();
                if ($model->save()) {
                    foreach ($organisasiCoach as $key => $value) {
                        $modelOrganisasiCoach = new OrganisasiCoach();
                        $modelOrganisasiCoach->id_coach = $model->id;
                        $modelOrganisasiCoach->keterangan = ($value['keterangan']==null ? null : $value['keterangan']);
                        $modelOrganisasiCoach->tahun_awal = ($value['tahun_awal']==null ? null : $value['tahun_awal']);
                        $modelOrganisasiCoach->tahun_akhir = ($value['tahun_akhir']==null ? null : $value['tahun_akhir']);
                        if (!is_null($modelOrganisasiCoach->tahun_akhir) && !is_null($modelOrganisasiCoach->keterangan) && !is_null($modelOrganisasiCoach->tahun_akhir)) {
                            $modelOrganisasiCoach->save();
                        }
                    }
                    foreach ($karirCoach as $key => $value) {
                        $modelKarir = new Karir();
                        $modelKarir->id_user = $modelUser->id;
                        $modelKarir->instansi = ($value['instansi']==null ? null : $value['instansi']);
                        $modelKarir->posisi = ($value['posisi']==null ? null : $value['posisi']);
                        $modelKarir->tahun_masuk = ($value['tahun_awal']==null ? null : $value['tahun_awal']);
                        $modelKarir->tahun_keluar = ($value['tahun_akhir']==null ? null : $value['tahun_akhir']);
                        $modelKarir->type='2';
                        // print_r($modelKarir->id_user);
                        if (!is_null($modelKarir->tahun_keluar) && !is_null($modelKarir->tahun_masuk) && !is_null($modelKarir->posisi) && !is_null($modelKarir->instansi)) {
                            print_r($modelKarir->save(false));
                        }
                    }
                    \Yii::$app->getSession()->setFlash('success', "<i class='fa fa-check-circle'></i> Pendaftaran Telah Berhasil");
                    return $this->redirect(['index']);
                }
            }
            // print_r($model);
        }else{
            return $this->renderAjax('_form', [
                'modelUser'=>$modelUser,
                'modelBiodataUser'=>$modelBiodataUser,
                'modelOrganisasiCoach'=>$modelOrganisasiCoach,
                'modelKarir'=>$modelKarir,
                'model' => $model,
            ]);
            
        }

    }

    /**
     * Updates an existing Coach model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Coach model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = 0;
        $model->save(false);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Coach model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Coach the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coach::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
