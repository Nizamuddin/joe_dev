<?php

namespace app\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kursus as KursusModel;

/**
 * Kursus represents the model behind the search form of `app\models\Kursus`.
 */
class Kursus extends KursusModel
{
    /**
     * {@inheritdoc}
     */

    
    public function rules()
    {
        return [
            [['id_mapel', 'nama', 'tanggal_awal', 'tanggal_akhir', 'quota', 'status'], 'required'],
            [['id_mapel', 'quota', 'status'], 'integer'],
            [['tanggal_awal', 'tanggal_akhir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KursusModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_mapel' => $this->id_mapel,
            'tanggal_start' => $this->tanggal_start,
            'tanggal_end' => $this->tanggal_end,
            'quota' => $this->quota,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
