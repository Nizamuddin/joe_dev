<?php

namespace app\models;
use Yii;

/**
 * This is the model class for table "mapel".
 *
 * @property int $id
 * @property string $nama
 * @property string $keterangan
 *
 * @property Kursus[] $kursuses
 */
class Mapel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mapel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'keterangan'], 'required'],
            [['keterangan'], 'string'],
            [['nama'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKursuses()
    {
        return $this->hasMany(Kursus::className(), ['id_mapel' => 'id']);
    }
}
