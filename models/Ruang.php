<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruang".
 *
 * @property int $id
 * @property string $nama
 * @property string $tempat
 * @property string $keterangan
 *
 * @property Jadwal[] $jadwals
 */
class Ruang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'tempat', 'keterangan'], 'required'],
            [['keterangan'], 'string'],
            [['nama'], 'string', 'max' => 50],
            [['tempat'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tempat' => 'Tempat',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::className(), ['id_ruangan' => 'id']);
    }
}
