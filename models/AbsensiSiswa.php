<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "absensi_siswa".
 *
 * @property int $id
 * @property int $id_siswa
 * @property int $id_jadwal
 * @property string $keterangan h,i,s,a
 * @property string $waktu
 *
 * @property Siswa $siswa
 * @property Jadwal $jadwal
 */
class AbsensiSiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'absensi_siswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_siswa', 'id_jadwal'], 'required'],
            [['id_siswa', 'id_jadwal'], 'integer'],
            [['waktu'], 'safe'],
            [['keterangan'], 'string', 'max' => 1],
            [['id_siswa'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['id_siswa' => 'id']],
            [['id_jadwal'], 'exist', 'skipOnError' => true, 'targetClass' => Jadwal::className(), 'targetAttribute' => ['id_jadwal' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_siswa' => 'Id Siswa',
            'id_jadwal' => 'Id Jadwal',
            'keterangan' => 'Keterangan',
            'waktu' => 'Waktu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'id_siswa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwal()
    {
        return $this->hasOne(Jadwal::className(), ['id' => 'id_jadwal']);
    }
}
