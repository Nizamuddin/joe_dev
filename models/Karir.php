<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "karir".
 *
 * @property int $id
 * @property int $id_user
 * @property string $instansi
 * @property string $posisi
 * @property string $tahun_masuk
 * @property string $tahun_keluar
 * @property int $type 1=siswa ; 2 = coach
 *
 * @property User $user
 */
class Karir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'karir';
    }
    public $karir;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'instansi', 'posisi', 'tahun_masuk', 'tahun_keluar', 'type'], 'required'],
            [['id_user', 'type'], 'integer'],
            [['instansi', 'posisi'], 'string', 'max' => 200],
            [['tahun_masuk', 'tahun_keluar'], 'string', 'max' => 15],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'instansi' => 'Instansi',
            'posisi' => 'Posisi',
            'tahun_masuk' => 'Tahun Masuk',
            'tahun_keluar' => 'Tahun Keluar',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
