<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "siswa".
 *
 * @property int $id
 * @property int $id_user
 * @property string $pendidikan_terakhir
 * @property string $jurusan
 * @property string $status 'mendaftar, diterima,proses, lulus'
 *
 * @property AbsensiSiswa[] $absensiSiswas
 * @property Pembayaran[] $pembayarans
 * @property User $user
 * @property SiswaKursus[] $siswaKursuses
 */
class Siswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'siswa';
    }

    public $fotoKtp;

    const PATH_ = 'web/upload/siswa/';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'pendidikan_terakhir', 'jurusan', 'status'], 'required'],
            [['id_user'], 'integer'],
            [['pendidikan_terakhir', 'status', 'foto_ktp'], 'string', 'max' => 20],
            [['jurusan'], 'string', 'max' => 50],
            [['fotoKtp'], 'file',  'extensions' => 'gif, jpg, jpeg, png'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'pendidikan_terakhir' => 'Pendidikan Terakhir',
            'jurusan' => 'Jurusan',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsensiSiswas()
    {
        return $this->hasMany(AbsensiSiswa::className(), ['id_siswa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getPembayarans()
    {
        return $this->hasMany(Pembayaran::className(), ['id_siswa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswaKursuses()
    {
        return $this->hasMany(SiswaKursus::className(), ['id_siswa' => 'id']);
    }

    public function getPathButktiBayar()
    {
        return $this->getPembayarans();
    }

    public function getPathFoto()
    {
        return $this->user->biodataUsers->foto;
    }

    public function getBasePath()
    {
        return self::PATH_;
    }

    public function getNamaSiswa()
    {
        return $this->user->biodataUsers->nama;
    }

    public function getArrayPendidikanTerakhir()
    {
        return [
            'SMA/SMK sederajat' => 'SMA/SMK sederajat',
            'diploma' =>'Diploma',
            'sarjana' =>'Sarjana',
            'Wed' =>'Rabu',
            'magister' =>'Magister',
        ];
    }

    public function checkWaktuPembayaran($value='')
    {
        $model = SiswaKursus::find()->where(['id_siswa'=>User::getIdSiswa()])->orderBy(['id'=> SORT_DESC])->one();
        $currentDate = date('Y-m-d');
        $currentDate=date_create($currentDate);
        $startDate=date_create($model->tgl_pilih);
        $diff=date_diff($currentDate,$startDate);
        if ($diff->d < 3) {
            return true;
        }else{
            return false;
        }
    }

    public function getSisaWaktuPembayaran()
    {
        $model = SiswaKursus::find()->where(['id_siswa'=>User::getIdSiswa()])->orderBy(['id'=> SORT_DESC])->one();
        $currentDate = date('Y-m-d');
        $currentDate=date_create($currentDate);
        $startDate=date_create($model->tgl_pilih);
        return date_diff($currentDate,$startDate)->d;
    }
}
