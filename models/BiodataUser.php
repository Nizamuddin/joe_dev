<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "biodata_user".
 *
 * @property int $id
 * @property int $id_user
 * @property string $nama
 * @property string $no_ktp
 * @property string $tempat_lahir
 * @property string $tgl_lahir
 * @property string $jenis_kelamin
 * @property string $alamat
 * @property string $hp
 * @property string $email
 * @property string $foto
 *
 * @property User $user
 */
class BiodataUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'biodata_user';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'nama', 'no_ktp', 'tempat_lahir', 'tgl_lahir', 'jenis_kelamin', 'alamat', 'hp', 'foto'], 'required'],
            [['id_user'], 'integer'],
            [['tgl_lahir'], 'safe'],
            [['alamat'], 'string'],
            [['nama', 'no_ktp', 'tempat_lahir'], 'string', 'max' => 50],
            [['jenis_kelamin'], 'string', 'max' => 15],
            ['hp', 'number'],
            [['file'], 'file',  'extensions' => 'gif, jpg, jpeg, png'],
            [['foto'], 'string', 'max' => 200],
            [['no_ktp'], 'unique', 'targetClass' => '\app\models\BiodataUser']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'nama' => 'Nama',
            'no_ktp' => 'No Ktp',
            'tempat_lahir' => 'Tempat Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'alamat' => 'Alamat',
            'hp' => 'Hp',
            'foto' => 'Foto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
