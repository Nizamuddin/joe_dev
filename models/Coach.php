<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coach".
 *
 * @property int $id
 * @property int $id_user
 * @property string $status_menikah
 * @property string $kewarganegaraan
 * @property string $pendidikan
 * @property int $status 1 = aktif; 0 = gak aktif
 *
 * @property AbsensiCoach[] $absensiCoaches
 * @property User $user
 * @property Jadwal[] $jadwals
 */
class Coach extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coach';
    }   
    const PATH_ = 'web/upload/coach/';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'status_menikah', 'kewarganegaraan', 'pendidikan', 'status'], 'required'],
            [['id_user', 'status'], 'integer'],
            [['pendidikan'], 'string'],
            [['status_menikah'], 'string', 'max' => 15],
            [['kewarganegaraan'], 'string', 'max' => 20],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'status_menikah' => 'Status Menikah',
            'kewarganegaraan' => 'Kewarganegaraan',
            'pendidikan' => 'Pendidikan',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsensiCoaches()
    {
        return $this->hasMany(AbsensiCoach::className(), ['id_coach' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::className(), ['id_coach' => 'id']);
    }
    
    public function getPathFoto()
    {
        return $this->user->biodataUsers->foto;
    }

    public function getClassLabel($value='')
    {
        if ($this->status == 0) {
            return 'primary';
        }elseif ($this->status ==1) {
            return 'success';
        }else{
            return 'danger';
        }    
    }

    public function getStatusString()
    {
        if ($this->status == 0) {
            return 'Proses';
        }elseif ($this->status ==1) {
            return 'Aktif';
        }else{
            return 'Tidak Aktif';
        }
    }
}
