<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "jadwal".
 *
 * @property int $id
 * @property int $id_kursus
 * @property int $id_coach
 * @property string $waktu_mulai
 * @property string $waktu_selesai
 * @property string $hari
 * @property int $id_ruangan
 *
 * @property AbsensiSiswa[] $absensiSiswas
 * @property Kursus $kursus
 * @property Coach $coach
 * @property Ruang $ruangan
 */
class Jadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kursus', 'id_coach', 'waktu_mulai', 'waktu_selesai', 'hari', 'id_ruangan'], 'required'],
            [['id_kursus', 'id_coach', 'id_ruangan'], 'integer'],
            [['waktu_mulai', 'waktu_selesai', 'hari'], 'safe'],
            [['mapel'], 'string', 'max'=>200],
            [['id_kursus'], 'exist', 'skipOnError' => true, 'targetClass' => Kursus::className(), 'targetAttribute' => ['id_kursus' => 'id']],
            [['id_coach'], 'exist', 'skipOnError' => true, 'targetClass' => Coach::className(), 'targetAttribute' => ['id_coach' => 'id']],
            [['id_ruangan'], 'exist', 'skipOnError' => true, 'targetClass' => Ruang::className(), 'targetAttribute' => ['id_ruangan' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kursus' => 'Id Kursus',
            'id_coach' => 'Coach',
            'waktu_mulai' => 'Waktu Mulai',
            'waktu_selesai' => 'Waktu Selesai',
            'hari' => 'Hari',
            'id_ruangan' => 'Ruangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsensiSiswas()
    {
        return $this->hasMany(AbsensiSiswa::className(), ['id_jadwal' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKursus()
    {
        return $this->hasOne(Kursus::className(), ['id' => 'id_kursus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoach()
    {
        return $this->hasOne(Coach::className(), ['id' => 'id_coach']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuangan()
    {
        return $this->hasOne(Ruang::className(), ['id' => 'id_ruangan']);
    }

    public function getAbsensi()
    {
        if (User::getType()==2) {
            $abs = AbsensiSiswa::find()->where(['id_jadwal'=>$this->id])->andWhere(['id_siswa'=>User::getIdSiswa()])->one();
            if ($abs) {
                if ($abs->keterangan=='h') {
                    echo '<span class="label label-success"><i class="fa fa-check"></i></span>';
                }else{
                    echo '<span class="label label-danger"><i class="fa fa-times"></i></span>';
                }
            }else{
                echo "-";

            }
        }else{
            $abs = AbsensiCoach::find()->where(['id_jadwal'=>$this->id])->andWhere(['id_coach'=>User::getIdCoach()])->one();
            if ($abs) {
                if ($abs->keterangan=='h') {
                    echo '<span class="label label-success"><i class="fa fa-check"></i></span>';
                }else{
                    echo '<span class="label label-danger"><i class="fa fa-times"></i></span>';
                }
            }else{
                echo "-";

            }
        }
    }

    public function getCheckAbsensi($idJadwal)
    {
        $abs = AbsensiCoach::find()->where(['id_jadwal'=>$idJadwal])->andWhere(['id_coach'=>User::getIdCoach()])->one();
        if ($abs) {
            return true;
        }else{
            return false;
        }
    }

    public static function getArrayCoach()
    {
        return ArrayHelper::map(Coach::find()->joinWith('user')->joinWith('user.biodataUsers')->where(['coach.status'=>1])->all(), 'id', 'user.biodataUsers.nama');
    }

    public static function getArrayRuang()
    {
        return ArrayHelper::map(Ruang::find()->all(), 'id', 'nama');
    }
}
