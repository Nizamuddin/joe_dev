<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisasi_coach".
 *
 * @property int $id
 * @property int $id_coach
 * @property string $keterangan
 * @property string $tahun_awal
 * @property string $tahun_akhir
 *
 * @property AbsensiCoach $coach
 */
class OrganisasiCoach extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organisasi_coach';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_coach', 'keterangan', 'tahun_awal', 'tahun_akhir'], 'required'],
            [['id_coach'], 'integer'],
            [['keterangan', 'tahun_awal', 'tahun_akhir'], 'string', 'max' => 200],
            [['id_coach'], 'exist', 'skipOnError' => true, 'targetClass' => Coach::className(), 'targetAttribute' => ['id_coach' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_coach' => 'Id Coach',
            'keterangan' => 'Keterangan',
            'tahun_awal' => 'Tahun Awal',
            'tahun_akhir' => 'Tahun Akhir',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoach()
    {
        return $this->hasOne(Coach::className(), ['id' => 'id_coach']);
    }
}
