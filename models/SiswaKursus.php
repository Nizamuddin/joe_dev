<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "siswa_kursus".
 *
 * @property int $id
 * @property int $id_siswa
 * @property int $id_kursus
 *
 * @property Siswa $siswa
 * @property Kursus $kursus
 */
class SiswaKursus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'siswa_kursus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_siswa', 'id_kursus', 'tgl_pilih'], 'required'],
            [['id_siswa', 'id_kursus'], 'integer'],
            [['id_siswa'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['id_siswa' => 'id']],
            [['id_kursus'], 'exist', 'skipOnError' => true, 'targetClass' => Kursus::className(), 'targetAttribute' => ['id_kursus' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_siswa' => 'Id Siswa',
            'id_kursus' => 'Id Kursus',
            'tgl_pilih' => 'Tanggal Memilih'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'id_siswa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKursus()
    {
        return $this->hasOne(Kursus::className(), ['id' => 'id_kursus']);
    }
}
