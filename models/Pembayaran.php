<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembayaran".
 *
 * @property int $id
 * @property int $id_siswa
 * @property double $nominal
 * @property string $jenis
 * @property string $catatan
 * @property string $status
 * @property string $bukti_transfer
 *
 * @property Siswa $siswa
 */
class Pembayaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pembayaran';
    }

    public $file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_siswa', 'nominal', 'jenis', 'catatan', 'status', 'bukti_transfer', 'atas_nama'], 'required'],
            [['id_siswa'], 'integer'],
            [['nominal'], 'number'],
            [['catatan', 'atas_nama'], 'string'],
            [['file'], 'file', 'extensions' => 'gif, jpg, jpeg, png'],
            [['jenis'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 20],
            [['bukti_transfer'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_siswa' => 'Id Siswa',
            'Nominal' => 'Nominal',
            'jenis' => 'Jenis',
            'catatan' => 'Catatan',
            'status' => 'Status',
            'atas_nama'=>'Atas Nama',
            'bukti_transfer' => 'Bukti Transfer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'id_siswa']);
    }

    public function getPath()
    {
        return $this->bukti_transfer;
    }

    public function getType()
    {
        return [
            'tunai' => 'Tunai',
            'transfer' => 'Transfer',
            'lain-lain'=> 'Lain-Lain'
        ];
    }

    public static function getIdUser(){
        return Yii::$app->user->identity->id;
    }

    public static function getIdSiswa($value='')
    {
        return Yii::$app->user->identity->siswas->id;
    }
}
