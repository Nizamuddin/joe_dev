<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id_user
 * @property string $username
 * @property string $password
 * @property int $status 1=akrif 0=non akrif
 * @property int $type 1 = admin ; 2 = siswa ; 3 = coach; 4 = SA
 *
 * @property BiodataUser[] $biodataUsers
 * @property Coach[] $coaches
 * @property Karir[] $karirs
 * @property Siswa[] $siswas
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public $newPassword;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'status', 'type'], 'required'],
            [['status', 'type'], 'integer'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 50],
            [['password', 'newPassword'], 'string', 'max' => 255],
            [['email'], 'unique', 'targetAttribute' => 'email', 'targetClass' => '\app\models\User', 'message' => 'email ini telah tersedia']
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
            'type' => 'Type',
        ];
    }

     /**
     * @inheritdoc
     */
    public static function findIdentity($ID_USER)
    {
        return self::findOne($ID_USER);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function getAuthKey(){
        return null;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return null;
    }

      /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id_user;
    }


    public static function findByEmail($email)
    {
        
        return self::findOne(['email' => $email]);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {   
        return Yii::$app->getSecurity()->validatePassword($password,$this->password);
        // return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBiodataUsers()
    {
        return $this->hasOne(BiodataUser::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaches()
    {
        return $this->hasOne(Coach::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKarirs()
    {
        return $this->hasMany(Karir::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswas()
    {
        return $this->hasOne(Siswa::className(), ['id_user' => 'id_user']);
    }

    public static function getType(){
        return Yii::$app->user->identity->type;
    }

    public static function getNama(){
        return Yii::$app->user->identity->biodataUsers->nama;
    }

    public static function getStatusSiswa()
    {
        return Yii::$app->user->identity->siswas->status;
    }

    public static function getIdSiswa()
    {
        return Yii::$app->user->identity->siswas->id;
    }

    public static function getIdCoach()
    {
        return Yii::$app->user->identity->coaches->id;
    }

    public static function getIdUser()
    {
        return Yii::$app->user->identity->id;
    }
}
