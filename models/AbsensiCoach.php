<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "absensi_coach".
 *
 * @property int $id
 * @property int $id_coach
 * @property int $id_jadwal
 * @property string $keterangan
 * @property string $waktu
 *
 * @property Coach $coach
 * @property OrganisasiCoach[] $organisasiCoaches
 */
class AbsensiCoach extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'absensi_coach';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_coach', 'id_jadwal', 'keterangan'], 'required'],
            [['id_coach', 'id_jadwal'], 'integer'],
            [['waktu'], 'safe'],
            [['keterangan'], 'string', 'max' => 200],
            [['id_coach'], 'exist', 'skipOnError' => true, 'targetClass' => Coach::className(), 'targetAttribute' => ['id_coach' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_coach' => 'Id Coach',
            'id_jadwal' => 'Id Jadwal',
            'keterangan' => 'Keterangan',
            'waktu' => 'Waktu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoach()
    {
        return $this->hasOne(Coach::className(), ['id' => 'id_coach']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisasiCoaches()
    {
        return $this->hasMany(OrganisasiCoach::className(), ['id_coach' => 'id']);
    }
}
