<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "kursus".
 *
 * @property int $id
 * @property int $id_mapel
 * @property string $nama
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property int $quota
 * @property int $status 1 = dibuka; 2 = sedang ; 3 = ditutup
 *
 * @property Jadwal[] $jadwals
 * @property Mapel $mapel
 * @property SiswaKursus[] $siswaKursuses
 */
class Kursus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    const DIBUKA = 1;
    const DIBUAT = 0;
    const SEDANG_BERLANGSUNG = 2;
    const DITUTUP = 3;

    public $jadwal;
    public static function tableName()
    {
        return 'kursus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'tanggal_awal', 'tanggal_akhir', 'quota', 'status'], 'required'],
            [['quota', 'status'], 'integer'],
            [['tanggal_awal', 'tanggal_akhir'], 'safe'],
            [['nama'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tanggal_awal' => 'Tanggal Awal',
            'tanggal_akhir' => 'Tanggal Akhir',
            'quota' => 'Quota',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::className(), ['id_kursus' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapel()
    {
        return $this->hasOne(Mapel::className(), ['id' => 'id_mapel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswaKursuses()
    {
        return $this->hasMany(SiswaKursus::className(), ['id_kursus' => 'id']);
    }

    public function saving()
    {
        $this->status = self::DIBUAT;
        return $this->save();
    }

    public function getArrayStatus($value='')
    {
        return [
            self::DIBUKA => 'Dibuka',
            self::SEDANG_BERLANGSUNG=>'Sedang Berlangsung',
            self::DITUTUP =>'Ditutup'
        ];
    }

    public function getArrayDays($value='')
    {
        return [
            'Sun' => 'Minggu',
            'Mon' =>'Senin',
            'Tue' =>'Selasa',
            'Wed' =>'Rabu',
            'Thu' =>'Kamis',
            'Fri' =>'Senin',
            'Sat' =>'Sabtu',
        ];
    }

    public function getArraySelectedDay($array)
    {
        $selected = array();
        $selected['days'] = array();
        $selected['coach'] = array();
        $selected['room'] = array();
        $selected['start'] = array();
        $selected['end'] = array();
        foreach ($array as $key => $value) {
            array_push($selected['days'], $value['days']);
            array_push($selected['coach'], $value['coach']);
            array_push($selected['room'], $value['ruang']);
            array_push($selected['start'], $value['tahun_awal']);
            array_push($selected['end'], $value['tahun_akhir']);
        }
        return $selected;
    }

    public function getClassLabel($value='')
    {
        if ($this->status == 1) {
            return 'primary';
        }elseif ($this->status ==2) {
            return 'success';
        }else{
            return 'danger';
        }    
    }

    public function getStatusString()
    {
        if ($this->status == 0) {
            return 'Belum Disetujui';
        }
        else if ($this->status == 1) {
            return 'Dibuka';
        }elseif ($this->status ==2) {
            return 'Sedang Berlangsung';
        }else{
            return 'Ditutup';
        }
    }

    public static function getArrayMapel()
    {
        return ArrayHelper::map(Mapel::find()->all(), 'id', 'nama');
    }

    public function getSisaQuota()
    {
        return ($this->quota - count($this->siswaKursuses));
    }
}
