<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Coach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coach';
$this->params['breadcrumbs'][] = $this->title;

Modal::begin([
        'header'=>'<h4>Create Tentor</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
echo "<div id='content-modal'></div>";
Modal::end();
$urlCreate = Url::toRoute(['/coach/create']);
?>
<div class="coach-index">
    <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
        }
    ?>
    <?php if (User::getType()==3) { ?>
        <?=Html::button('<i class="fa fa-plus"></i>', ['value'=>$urlCreate,'class' => 'btn btn-primary btn-flat btn-modal btn-sm']);?>
    <?php } ?>
    <div class="block full">
        <div class="block-title">
            <h2>Datatables</h2>
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">ID</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th style="width: 120px;">Email</th>
                        <th>Status</th>
                        <th class="text-center" style="width: 100px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataCoach as $key => $value) { 
                        $urlView = Url::toRoute(['/coach/view', 'id' => $value->id]);
                    ?>
                    <tr>
                        <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
                        <td><strong><?=strtoupper($value->user->biodataUsers->nama)?></strong></td>
                        <td><?=$value->user->biodataUsers->alamat?></td>
                        <td><?=$value->user->email?></td>
                        <td><span class="label label-<?=$value->classLabel?>"><?=$value->statusString?></span></td>
                        <td class="text-center">
                            <?=Html::button('<i class="fa fa-info-circle"></i>', ['value'=>$urlView,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal']);?>
                            <?= Html::a('<i class="fa fa-times"></i>', ['delete', 'id' => $value->id], [
                                'class' => 'btn btn-effect-ripple btn-xs btn-danger',
                                 'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],]) ?>
                            <?php if (User::getType()==4) { ?>
                                <?= Html::a('<i class="gi gi-check"></i>', ['verifikasi', 'id' => $value->id], [
                                    'class' => 'btn btn-effect-ripple btn-xs btn-success',
                                     'data' => [
                                        'confirm' => 'Apakah anda yakin?',
                                        'method' => 'post',
                                    ],]) 
                                ?>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>$(function(){ UiTables.init(); });</script>