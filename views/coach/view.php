<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dmstr\bootstrap\Tabs;
/* @var $this yii\web\View */
/* @var $model app\models\Coach */

$this->title = strtoupper($model->user->biodataUsers->nama);
$this->params['breadcrumbs'][] = ['label' => 'Coaches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coach-view">

    <div class="widget-image widget-image-sm">
        <img src="<?=Yii::$app->homeUrl?>templates/img/placeholders/photos/photo1@2x.jpg" alt="image">
        <div class="widget-image-content text-center">
            <img src="<?=Yii::$app->homeUrl?>/<?=$model->pathFoto?>" alt="avatar" class="img-circle img-thumbnail img-thumbnail-transparent img-thumbnail-avatar-2x push">
            <h2 class="widget-heading text-light"><strong><?=$model->user->biodataUsers->nama?></strong></h2>
            <h4 class="widget-heading text-light-op"><em><?=$model->user->email?></em></h4>
        </div>
    </div>
    <br>
    <?php $this->beginBlock('profile'); ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label'=>'No Ktp',
                'value'=>$model->user->biodataUsers->no_ktp,
            ],
            [
                'label'=>'Tempat dan Tanggal Lahir',
                'value'=>function($model){
                    return $model->user->biodataUsers->tempat_lahir.', '.$model->user->biodataUsers->tgl_lahir;
                },
            ],
            [
                'label'=>'Jenis Kelamin',
                'value'=>$model->user->biodataUsers->jenis_kelamin,
            ],
            [
                'label'=>'Alamat',
                'value'=>$model->user->biodataUsers->alamat,
            ],
            [
                'label'=>'No Telpn',
                'value'=>$model->user->biodataUsers->hp,
            ],
            
            'status_menikah',
            'kewarganegaraan',
            'pendidikan:ntext',
        ],
    ]) ?>
    <?php $this->endBlock() ?>
    
    <?php $this->beginBlock('pengalaman-kerja'); ?>
    <div class="row">
        <?php 
            foreach ($dataPengalamanKerja as $key => $value) {?>
                <div class="col-sm-4">
                    <a href="javascript:void(0)" class="widget">
                        <div class="widget-content widget-content-mini themed-background-dark-social">
                            <strong class="text-light-op"><?=$value->tahun_masuk?>-<?=$value->tahun_keluar?></strong>
                        </div>
                        <div class="widget-content themed-background-social clearfix">
                            <div class="widget-icon pull-right">
                                <i class="fa fa-building-o text-light-op"></i>
                            </div>
                            <h2 class="widget-heading h3 text-light"><strong><?=$value->instansi?></strong></h2>
                            <span class="text-light-op"><?=$value->posisi?></span>
                        </div>
                    </a>
                </div>
        <?php } ?>
    </div>
    <?php $this->endBlock() ?>

    <?php $this->beginBlock('pengalaman-organisasi'); ?>
    <div class="row">
        <?php 
            foreach ($dataOrganisasi as $key => $value) {?>
                <div class="col-sm-4">
                    <a href="javascript:void(0)" class="widget">
                        <div class="widget-content widget-content-mini themed-background-dark-social">
                            <strong class="text-light-op"><?=$value->tahun_awal?>-<?=$value->tahun_akhir?></strong>
                        </div>
                        <div class="widget-content themed-background-social clearfix">
                            <div class="widget-icon pull-right">
                                <i class="fa fa-building-o text-light-op"></i>
                            </div>
                            <h2 class="widget-heading h3 text-light"><strong><?=$value->keterangan?></strong></h2>
                        </div>
                    </a>
                </div>
        <?php } ?>
    </div>
    <?php $this->endBlock() ?>
    
    <?= Tabs::widget(
        [
            'id' => 'relation-tabs',
            'encodeLabels' => false,
            'items' => [ 
                [
                    'label'   => '<b class="">Profile</b>',
                    'content' => $this->blocks['profile'],
                    'active'  => true,
                ],
                [
                    'label'   => '<b class="">Pengalaman Kerja</b>',
                    'content' => $this->blocks['pengalaman-kerja'],
                    'active'  => false,
                ],
                [
                    'label'   => '<b class="">Pengalaman Organisasi</b>',
                    'content' => $this->blocks['pengalaman-organisasi'],
                    'active'  => false,
                ]

            ]
        ]
    );?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal-header').html('<h4>Profile<h4>');
    });
</script>