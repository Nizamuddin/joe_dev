<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\Coach */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile("@web/web/templates/js/pages/formsWizard.js", ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("@web/web/templates/css/custom.css");
?>

<div class="block">
    <!-- Clickable Wizard Title -->
    <!-- END Clickable Wizard Title -->

    <!-- Clickable Wizard Content -->
    <?php $form = ActiveForm::begin([
        'id' => 'clickable-wizard',
        'class'=>'form-horizontal form-bordered',
        'enableAjaxValidation'=>true,
        'validationUrl'=>Url::toRoute('/site/username-validation'),
        'enableClientValidation'=>true,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]

    ]); ?>
        
        <div id="clickable-first" class="step">
            <!-- Step Info -->
            <div class="form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified clickable-steps">
                        <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-first"><i class="fa fa-user"></i> <strong>Account</strong></a></li>
                        <li><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-pencil-square-o"></i> <strong>Profile</strong></a></li>
                        <li class=""><a href="javascript:void(0)" data-gotostep="clickable-third"><i class="fa fa-check"></i> <strong>Organisasi dan Pengalaman Kerja</strong></a></li>
                    </ul>
                </div>
            </div>
            <!-- END Step Info -->

            <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true]) ?>
            <?=
                $form->field($modelBiodataUser, 'file')->widget(FileInput::classname(), [
                    'options' => [
                        'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
                    ],

                    'language' => 'en',
                    'pluginOptions' => [
                        'browseClass' => 'btn btn-success',
                        'browseLabel' =>  'Unggah',
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'required'  =>true
                        // 'mainClass' => 'btn-loginregist'
                    ],
                ])->label('Foto')?>
            
        </div>
        <!-- END First Step -->

        <!-- Second Step -->
        <div id="clickable-second" class="step">
            <!-- Step Info -->
            <div class="form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified clickable-steps">
                        <li><a href="javascript:void(0)" class="text-muted" data-gotostep="clickable-first"><i class="fa fa-user"></i> <del><strong>Account</strong></del></a></li>
                        <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-pencil-square-o"></i> <strong>Profile</strong></a></li>
                        <li class=""><a href="javascript:void(0)" data-gotostep="clickable-third"><i class="fa fa-check"></i> <strong>Organisasi dan Pengalaman Kerja</strong></a></li>
                    </ul>
                </div>
            </div>
            <!-- END Step Info -->
            <?= $form->field($modelBiodataUser, 'nama')->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelBiodataUser, 'no_ktp')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-md-6">
                    
                    <?= $form->field($modelBiodataUser, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($modelBiodataUser, 'tgl_lahir')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                     ?>
                    
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($modelBiodataUser, 'jenis_kelamin')->dropDownList([null=>'','laki-laki' => 'Laki-Laki', 'Perempuan' => 'Perempuan']); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'status_menikah')->dropDownList([null=>'','sudah' => 'Sudah Menikah', 'belum' => 'Belum Menikah']); ?>
                    
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    
                    <?= $form->field($model, 'kewarganegaraan')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                   <?= $form->field($modelBiodataUser, 'hp')->textInput(['maxlength' => true]) ?>
                    
                </div>

            </div>

            

            <?= $form->field($modelBiodataUser, 'alamat')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'pendidikan')->textarea(['rows' => 6]) ?>

        </div>
        <!-- END Second Step -->

        <div id="clickable-third" class="step">
            <!-- Step Info -->
            <div class="form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified clickable-steps">
                        <li><a href="javascript:void(0)" class="text-muted" data-gotostep="clickable-first"><i class="fa fa-user"></i> <del><strong>Account</strong></del></a></li>
                        <li><a href="javascript:void(0)" class="text-muted" data-gotostep="clickable-second"><i class="fa fa-pencil-square-o"></i> <del><strong>Profile</strong></del></a></li>
                        <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-third"><i class="fa fa-check"></i> <strong>Organisasi dan Pengalaman Kerja</strong></a></li>
                    </ul>
                </div>
            </div>
            <!-- END Step Info -->
            <?php
                echo $form->field($modelOrganisasiCoach, 'organisasi')->widget(MultipleInput::className(), [
                    'max'               => 6,
                    'min'               => 1, // should be at least 2 rows
                    'allowEmptyList'    => false,
                    'enableGuessTitle'  => true,
                    'options'           =>[
                        'class'             =>'asdsa',
                    ],
                    'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
                    'columns' => [
                            [
                                'name'  => 'keterangan',
                                'title' => 'User',
                            ],
                            [
                                'name'  => 'tahun_awal',
                                'type'  => \kartik\date\DatePicker::className(),
                                'title' => 'Awal',
                                'value' => function($data) {
                                    return $data['day'];
                                },
                                'options' => [
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd',
                                        'todayHighlight' => true
                                    ]
                                ]
                            ],
                            [
                                'name'  => 'tahun_akhir',
                                'type'  => \kartik\date\DatePicker::className(),
                                'title' => 'Akhir',
                                'value' => function($data) {
                                    return $data['day'];
                                },
                                'items' => [
                                    '0' => 'Saturday',
                                    '1' => 'Monday'
                                ],
                                'options' => [
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd',
                                        'todayHighlight' => true
                                    ]
                                ]
                            ],
                        ]
                ])
                ->label('Organisasi');
            ?>

            <hr>
            <?php
                echo $form->field($modelKarir, 'karir')->widget(MultipleInput::className(), [
                    'max'               => 6,
                    'min'               => 1, // should be at least 2 rows
                    'allowEmptyList'    => false,
                    'enableGuessTitle'  => true,
                    'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
                    'columns' => [
                            [
                                'name'  => 'instansi',
                                'title' => 'Instansi',
                            ],
                            [
                                'name'  => 'posisi',
                                'title' => 'Posisi',
                            ],
                            [
                                'name'  => 'tahun_awal',
                                'type'  => \kartik\date\DatePicker::className(),
                                'title' => 'Tahun Masuk',
                                'value' => function($data) {
                                    return $data['day'];
                                },
                                'items' => [
                                    '0' => 'Saturday',
                                    '1' => 'Monday'
                                ],
                                'options' => [
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd',
                                        'todayHighlight' => true
                                    ]
                                ]
                            ],
                            [
                                'name'  => 'tahun_akhir',
                                'type'  => \kartik\date\DatePicker::className(),
                                'title' => 'Tahun Keluar',
                                'value' => function($data) {
                                    return $data['day'];
                                },
                                'items' => [
                                    '0' => 'Saturday',
                                    '1' => 'Monday'
                                ],
                                'options' => [
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd',
                                        'todayHighlight' => true
                                    ]
                                ]
                            ],
                        ]
                ])
                ->label('Pengalaman Kerja');
            ?>
        </div>

        <!-- Form Buttons -->
        <div class="form-group form-actions">
            <div class="col-md-8 col-md-offset-4">
                <button type="reset" class="btn btn-effect-ripple btn-danger" id="back1">Back</button>
                <button type="submit" class="btn btn-effect-ripple btn-primary" id="next1">Next</button>
            </div>
        </div>
        <!-- END Form Buttons -->
    <?php ActiveForm::end(); ?>
    <!-- END Clickable Wizard Content -->
</div>

<script src="<?=Yii::$app->homeUrl?>web/templates/js/pages/formsWizard.js"></script>
<script>$(function(){ FormsWizard.init(); });</script>