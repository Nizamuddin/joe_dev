<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Mapel */
/* @var $dataProvider yii\data\ActiveDataProvider */

Modal::begin([
        'header'=>'<h4>Create Mapel</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
echo "<div id='content-modal'></div>";
Modal::end();

$this->title = 'Ruangan';
$this->params['breadcrumbs'][] = $this->title;
// $this->registerJsFile("@web/templates/js/custom.js", ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("@web/templates/js/form.js", ['depends' => 'yii\web\JqueryAsset']);
// $this->registerJsFile("@web/templates/js/pages/uiTables.js", ['depends' => 'yii\web\JqueryAsset']);
$urlCreate = Url::toRoute(['/ruang/create']);
?>
<div class="mapel-index">

    <?=Html::button('<i class="fa fa-plus"></i>', ['value'=>$urlCreate,'class' => 'btn btn-primary btn-flat btn-modal btn-sm']);?>
    <div class="block full">
        <div class="block-title">
            <h2>Datatables</h2>
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">ID</th>
                        <th>Nama</th>
                        <th>Tempat</th>
                        <th>Keterangan</th>
                        <th class="text-center" style="width: 75px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataRuangan as $key => $value) { 
                        $urlView = Url::toRoute(['/ruang/update', 'id' => $value->id]);
                    ?>
                        <tr>
                            <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
                            <td><strong><?=strtoupper($value->nama)?></strong></td>
                            <td><?=$value->tempat?></td>
                            <td><?=$value->keterangan?></td>
                            <td class="text-center">
                                <?=Html::button('<i class="fa fa-pencil"></i>', ['value'=>$urlView,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal']);?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>$(function(){ UiTables.init(); });</script>