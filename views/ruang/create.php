<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ruang */

$this->title = 'Create Ruang';
$this->params['breadcrumbs'][] = ['label' => 'Ruangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ruang-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal-header').html('<h4>Create Ruangan<h4>');
    });
</script>