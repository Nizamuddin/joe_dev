<?php

/* @var $this \yii\web\View */
/* @var $content string */
use app\models\User;
?>

<ul class="sidebar-nav">
    <li>
        <a href="<?=Yii::$app->homeUrl?>kursus" class=" active"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Jadwal</span></a>
    </li>
    <li>
        <a href="<?=Yii::$app->homeUrl?>karir" class="<?= Yii::$app->controller->id == 'karir' ? 'active' : ''?>"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Karir</span></a>
    </li>
</ul>