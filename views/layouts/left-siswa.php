<?php

/* @var $this \yii\web\View */
/* @var $content string */
use app\models\User;
?>

<ul class="sidebar-nav">
    <li>
        <a href="<?=Yii::$app->homeUrl?>" class="<?= Yii::$app->controller->id == 'site' ? 'active' : ''?>"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
    </li>
    <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
    </li>
    <!-- <li>
        <a href="<?=Yii::$app->homeUrl?>pembayaran/create" class="<?= Yii::$app->controller->id == 'pembayaran' ? 'active' : ''?>"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Pembayaran</span></a>
    </li> -->
    <li>
        <a href="<?=Yii::$app->homeUrl?>kursus" class="<?= Yii::$app->controller->id == 'kursus' ? 'active' : ''?>"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Kursus</span></a>
    </li>
    
    <li>
        <a href="<?=Yii::$app->homeUrl?>karir" class="<?= Yii::$app->controller->id == 'karir' ? 'active' : ''?>"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Karir</span></a>
    </li>    
</ul>