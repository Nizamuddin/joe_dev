<ul class="sidebar-nav">
    <li>
        <a href="<?=Yii::$app->homeUrl?>" class=" active"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
    </li>
    <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
    </li>
    <li>
        <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-rocket sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Kursus</span></a>
        <ul>
            <li>
                <a href="<?=Yii::$app->homeUrl?>coach">Daftar Coach</a>
            </li>
            <li>
                <a href="<?=Yii::$app->homeUrl?>kursus">Daftar Kursus</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-airplane sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Siswa</span></a>
        <ul>
            <li>
                <a href="<?=Yii::$app->homeUrl?>siswa">Daftar Siswa</a>
            </li>
            <li>
                <a href="<?=Yii::$app->homeUrl?>pembayaran">Pembayaran</a>
            </li>
        </ul>
    </li>
</ul>