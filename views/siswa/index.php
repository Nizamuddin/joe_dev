<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Siswa;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Siswa';
$this->params['breadcrumbs'][] = $this->title;

Modal::begin([
        'header'=>'<h4>Detail Siswa</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
echo "<div id='content-modal'></div>";
Modal::end();

// $this->registerJsFile("@web/templates/js/custom.js", ['depends' => 'yii\web\JqueryAsset']);

?>
<div class="siswa-index">
    <div class="block full">
        <div class="block-title">
            <h2>Datatables</h2>
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th style="width: 80px;" class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th style="width: 320px;">Email</th>
                        <th style="width: 120px;" class="text-center"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataSiswa as $key => $value) { 
                        $urlView = Url::toRoute(['/siswa/view', 'id' => $value->id]);
                    ?>
                    <tr>
                        <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
                        <td><strong><?=strtoupper($value->user->biodataUsers->nama)?></strong></td>
                        <td><?=strtoupper($value->user->biodataUsers->alamat)?></td>
                        <td><?=strtoupper($value->user->email)?></td>
                        <td class="text-center">
                            <?=Html::button('<i class="fa fa-info-circle"></i>', ['value'=>$urlView,'class' => 'btn btn-primary btn-flat btn-modal btn-sm']);?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>$(function(){ UiTables.init(); });</script>