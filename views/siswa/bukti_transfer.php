<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Siswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="widget">
            <div class="widget-image widget-image-sm">
                <img src="<?=Yii::$app->homeUrl?>templates/img/placeholders/photos/photo1@2x.jpg" alt="image">
                <div class="widget-image-content text-center">
                    <img src="<?=Yii::$app->homeUrl?>upload/<?=$model->foto?>" alt="avatar" class="img-circle img-thumbnail img-thumbnail-transparent img-thumbnail-avatar-2x push">
                    <h2 class="widget-heading text-light"><strong><?=$model->nama?></strong></h2>
                    <h4 class="widget-heading text-light-op"><em><?=$model->email?></em></h4>
                </div>
            </div>
            <div class="widget-content widget-content-full border-bottom">
                <div class="row text-center">
                    <div class="col-xs-6 push-inner-top-bottom border-right">
                        <h3 class="widget-heading"><i class="gi gi-heart text-danger push"></i> <br><small><strong>1.5k</strong> Favorites</small></h3>
                    </div>
                    <div class="col-xs-6 push-inner-top-bottom">
                        <h3 class="widget-heading"><i class="gi gi-group themed-color-social push"></i> <br><small><strong>58.6k</strong> Followers</small></h3>
                    </div>
                </div>
            </div>
            <div class="widget-content border-bottom">
               <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'no_ktp',
                        'tempat_lahir',
                        'tgl_lahir',
                        'jenis_kelamin',
                        'alamat:ntext',
                        'hp',
                        'pendidikan_terakhir',
                        'jurusan',
                        'status',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

