<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Siswa';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">
    <div class="block">
        <!-- Table Styles Title -->
        <div class="block-title clearfix">
            <!-- Changing classes functionality initialized in js/pages/tablesGeneral.js -->
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default active" id="style-striped" data-toggle="tooltip" title=".table-striped">Striped</a>
                <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default" id="style-condensed" data-toggle="tooltip" title=".table-condensed">Condensed</a>
                <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default" id="style-hover" data-toggle="tooltip" title=".table-hover">Hover</a>
            </div>
            <div class="block-options pull-right">
                <div id="style-borders" class="btn-group">
                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default" id="style-default" data-toggle="tooltip">Default</a>
                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default active" id="style-bordered" data-toggle="tooltip" title=".table-bordered">Bordered</a>
                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default" id="style-borderless" data-toggle="tooltip" title=".table-borderless">Borderless</a>
                </div>
            </div>
            <h2><span class="hidden-xs">Table</span> Styles</h2>
        </div>
        <!-- END Table Styles Title -->

        <!-- Table Styles Content -->
        <div class="table-responsive">
            <table id="general-table" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th style="width: 80px;" class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th style="width: 320px;">Email</th>
                        <th style="width: 120px;" class="text-center"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataSiswa as $key => $value) { ?>
                    <tr>
                        <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
                        <td><strong>Gabriel Morris</strong></td>
                        <td>gabriel.morris@example.com</td>
                        <td>
                            <div class="progress progress-mini active remove-margin">
                                <div class="progress-bar progress-bar-striped progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"></div>
                            </div>
                        </td>
                        <td class="text-center">
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Edit User" class="btn btn-effect-ripple btn-sm btn-success"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Delete User" class="btn btn-effect-ripple btn-sm btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- END Table Styles Content -->
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>$(function(){ UiTables.init(); });</script>