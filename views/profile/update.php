<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use kartik\date\DatePicker;
use app\models\User;
$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siswa-index">
<div class="block full">
    <div class="block-title">
        <h2>Profile User</h2>
    </div>
        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'action' =>['update-biodate'],
        ]); ?>
        <?= $form->field($modelBioData, 'nama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelBioData, 'no_ktp')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelBioData, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelBioData, 'tgl_lahir')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter birth date ...'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-m-d'
            ]
        ]);
         ?>

        <?= $form->field($modelBioData, 'jenis_kelamin')->dropDownList(
                    ['laki-laki' => 'Laki-Laki', 'Perempuan' => 'Perempuan']
            ); ?>

        <?= $form->field($modelBioData, 'alamat')->textarea(['rows' => 6]) ?>

        <?= $form->field($modelBioData, 'hp')->textInput(['maxlength' => true]) ?>

        <?php if (User::getType()==2) {?>
        <?= $form->field($model, 'pendidikan_terakhir')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'jurusan')->textInput(['maxlength' => true]) ?>
		<?php }else{ ?>
        <?= $form->field($model, 'status_menikah')->dropDownList([null=>'','sudah' => 'Sudah Menikah', 'belum' => 'Belum Menikah']); ?>
        <?= $form->field($model, 'kewarganegaraan')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'pendidikan')->textarea(['rows' => 6]) ?>

        <?php } ?>
        <?=
            $form->field($modelBioData, 'file')->widget(FileInput::classname(), [
                'options' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
                ],
                'language' => 'en',
                'pluginOptions' => [
                    'initialPreview' => [
                        $modelBioData->foto ? Html::img(\Yii::$app->urlManager->createUrl($modelBioData->foto), [
                                    'style'=>'width: 100%;height: 100%'
                                ]) : null,
                    ],
                    'browseClass' => 'btn btn-success',
                    'browseLabel' =>  'Unggah',
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                ],
            ])->label('Foto')?>
         <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
         <?php ActiveForm::end(); ?>
</div>

<div class="block full">
    <div class="block-title">
        <h2>Account User</h2>
    </div>
        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'action' =>['update-account'],
        ]); ?>
        <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelUser, 'newPassword')->passwordInput() ?>
         <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
         <?php ActiveForm::end(); ?>
</div>
</div>