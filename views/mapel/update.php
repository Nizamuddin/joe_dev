<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mapel */

$this->title = 'Update Mapel: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mapels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mapel-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal-header').html('<h4>Update Mapel<h4>');
    });
</script>