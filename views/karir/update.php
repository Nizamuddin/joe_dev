<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Karir */

$this->title = 'Update Karir: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Karirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="karir-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal-header').html('<h4>Pengalaman Kerja<h4>');
    });
</script>