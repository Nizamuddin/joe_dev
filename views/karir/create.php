<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Karir */

$this->title = 'Create Karir';
$this->params['breadcrumbs'][] = ['label' => 'Karirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="karir-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal-header').html('<h4>Pengalaman Kerja<h4>');
    });
</script>