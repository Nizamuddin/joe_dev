<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Karir';
$this->params['breadcrumbs'][] = $this->title;
Modal::begin([
        'header'=>'<h4>Create Mapel</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
echo "<div id='content-modal'></div>";
Modal::end();
$urlCreate = Url::toRoute(['/karir/create']);
// $this->registerJsFile("@web/templates/js/custom.js", ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="karir-index">

    <?=Html::button('<i class="fa fa-plus"></i>', ['value'=>$urlCreate,'class' => 'btn btn-primary btn-flat btn-modal btn-sm']);?>
    <div class="block full">
        <div class="block-title">
            <h2>Datatables</h2>
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">ID</th>
                        <th>instansi</th>
                        <th>Posisi</th>
                        <th>tahun_masuk</th>
                        <th>tahun_keluar</th>
                        <th class="text-center" style="width: 75px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataKarir as $key => $value) { 
                        $urlUpdate = Url::toRoute(['/karir/update', 'id' => $value->id]);
                    ?>
                        <tr>
                            <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
                            <td><strong><?=strtoupper($value->instansi)?></strong></td>
                            <td><?=$value->posisi?></td>
                            <td><?=$value->tahun_masuk?></td>
                            <td><?=$value->tahun_keluar?></td>
                            <td class="text-center">
                                <?=Html::button('<i class="fa fa-pencil"></i>', ['value'=>$urlUpdate,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal']);?>
                                <?= Html::a('<i class="fa fa-times"></i>', ['delete', 'id' => $value->id], [
                                        'class' => 'btn btn-effect-ripple btn-xs btn-danger',
                                         'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>$(function(){ UiTables.init(); });</script>