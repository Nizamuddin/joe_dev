<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\Karir */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="karir-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        if ($model->isNewRecord) {
            
            echo $form->field($model, 'karir')->widget(MultipleInput::className(), [
                'max'               => 6,
                'min'               => 1, // should be at least 2 rows
                'allowEmptyList'    => false,
                'enableGuessTitle'  => true,
                'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
                'columns' => [
                        [
                            'name'  => 'instansi',
                            'title' => 'Instansi',
                        ],
                        [
                            'name'  => 'posisi',
                            'title' => 'Posisi',
                        ],
                        [
                            'name'  => 'tahun_awal',
                            'type'  => \kartik\date\DatePicker::className(),
                            'title' => 'Tahun Masuk',
                            'value' => function($data) {
                                return $data['day'];
                            },
                            'items' => [
                                '0' => 'Saturday',
                                '1' => 'Monday'
                            ],
                            'options' => [
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                ]
                            ]
                        ],
                        [
                            'name'  => 'tahun_akhir',
                            'type'  => \kartik\date\DatePicker::className(),
                            'title' => 'Tahun Keluar',
                            'value' => function($data) {
                                return $data['day'];
                            },
                            'items' => [
                                '0' => 'Saturday',
                                '1' => 'Monday'
                            ],
                            'options' => [
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                ]
                            ]
                        ],
                    ]
            ])
            ->label('Pengalaman Kerja');
        }else{
            echo $form->field($model, 'instansi')->textInput();
            echo $form->field($model, 'posisi')->textInput();
            echo $form->field($model, 'tahun_masuk')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);

            echo $form->field($model, 'tahun_keluar')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
        }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
