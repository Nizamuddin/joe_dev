<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use kartik\date\DatePicker;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
// echo Yii::getAlias('@web');
// die;
?>

<div id="login" class="">
    <div id="login-container">
            <!-- Login Header -->
        <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
            <i class="fa fa-cube text-light-op"></i> <strong>AppUI</strong>
        </h1>
        <?php
            foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
            }
        ?>
        <!-- END Login Header -->

        <!-- Login Block -->
        <div class="block animation-fadeInQuick">
            <!-- Login Title -->
            <div class="block-title">
                <div class="block-options pull-right">
                    <a href="#" class="btn btn-effect-ripple btn-primary" id="btn-regist" data-toggle="tooltip" data-placement="left" title="Create new account"><i class="fa fa-plus"></i></a>
                </div>
                <h2>Please Login</h2>
            </div>
            <!-- END Login Title -->
            <?php $form = ActiveForm::begin([
                'id' => 'form-login',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-xs-12\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>

                <?= $form->field($modelLogin, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($modelLogin, 'password')->passwordInput() ?>


               <div class="form-group form-actions">
                    <div class="col-xs-8">
                        <label class="csscheckbox csscheckbox-primary">
                            <input type="checkbox" id="login-remember-me" name="login-remember-me"><span></span> Remember Me?
                        </label>
                    </div>
                    <div class="col-xs-4 text-right">
                        <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Log In</button>
                        <?= Html::a('Regist', ['#'], ['class'=>'btn btn-effect-ripple btn-sm btn-primary']) ?>
                        <!-- <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Log In</button> -->
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            <!-- Social Login -->
            
          
            <!-- END Social Login -->
        </div>
        <!-- END Login Block -->

        <!-- Footer -->
        <footer class="text-muted text-center animation-pullUp">
            <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/RcsdAh" target="_blank">AppUI 2.9</a></small>
        </footer>
        <!-- END Footer -->
    </div>
</div>

<div id="user" class="">
    <div id="regist-container">
        <!-- Register Header -->
        <h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
            <a href="#" class="btn btn-effect-ripple btn-primary" id="btn-login" data-toggle="tooltip" data-placement="left" title="Create new account"><i class="fa fa-plus"></i></a> <strong>Create Account</strong>
        </h1>
        <!-- END Register Header -->
                    <?php $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'form-horizontal',
                                'role' => "form"
                            ],
                        'action' => ['site/regist'],
                        'enableAjaxValidation'=>true,
                        // 'validationUrl'=>Url::toRoute('/site/username-validation'),
                        'enableClientValidation'=>true,
                    ]); ?>
        <div class="row">
            <div class="col-md-6">
                <div class="block animation-fadeInQuickInv">

                    <?= $form->field($modelUser, 'email', ['enableAjaxValidation' => true, 'validateOnChange' => true])->textInput(['maxlength' => true]) ?>

                    <?= $form->field($modelUser, 'password')->passwordInput(['maxlength' => true]) ?>

                    <?= $form->field($modelBioData, 'nama')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($modelBioData, 'no_ktp', ['enableAjaxValidation' => true, 'validateOnChange' => true])->textInput(['maxlength' => true]) ?>

                    <?=
                        $form->field($modelSiswa, 'fotoKtp')->widget(FileInput::classname(), [
                            'options' => [
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
                            ],
                            'language' => 'en',
                            'pluginOptions' => [
                                'browseClass' => 'btn btn-success',
                                'browseLabel' =>  'Unggah',
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false,
                                'required'=>true,
                            ],
                        ])->label('File Ktp')?>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($modelBioData, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
                            
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($modelBioData, 'tgl_lahir')->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => 'Enter birth date ...'],
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-m-d'
                                ]
                            ]);
                             ?>
                            
                        </div>
                    </div>


                    <?= $form->field($modelBioData, 'jenis_kelamin')->dropDownList(
                                ['laki-laki' => 'Laki-Laki', 'Perempuan' => 'Perempuan']
                        ); ?>

                    <!-- END Register Form -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="block animation-fadeInQuickInv">
                    <?= $form->field($modelBioData, 'alamat')->textarea(['rows' => 6]) ?>
                    <!-- Register Title -->
                    <!-- END Register Title -->

                    <?= $form->field($modelBioData, 'hp')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($modelSiswa, 'pendidikan_terakhir')->dropDownList(
                            $modelSiswa->arrayPendidikanTerakhir
                    ); ?>

                    <?= $form->field($modelSiswa, 'jurusan')->textInput(['maxlength' => true]) ?>

                    <?=
                        $form->field($modelBioData, 'file')->widget(FileInput::classname(), [
                            'options' => [
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
                            ],
                            'language' => 'en',
                            'pluginOptions' => [
                                'browseClass' => 'btn btn-success',
                                'browseLabel' =>  'Unggah',
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false,
                                'required'=>true,
                            ],
                        ])->label('Foto')?>

                    

                    <div class="form-group">
                        <?= Html::submitButton($modelUser->isNewRecord ? 'Create' : 'Update', ['class' => $modelUser->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <!-- END Register Form -->
                </div>
            </div>
                    <?php ActiveForm::end(); ?>
        </div>
        <!-- Register Form -->
        
        <!-- END Register Block -->

        <!-- Footer -->
        <footer class="text-muted text-center animation-pullUp">
            <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/RcsdAh" target="_blank">AppUI 2.9</a></small>
        </footer>
        <!-- END Footer -->
    </div>
    

</div>

