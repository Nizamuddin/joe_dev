<?php

use yii\helpers\Html;
use app\models\User;
use app\models\Siswa;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Search\Kursus */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kursus';
$this->params['breadcrumbs'][] = $this->title;
Modal::begin([
        'header'=>'<h4>Create Kursus</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
    ]);
echo "<div id='content-modal'></div>";
Modal::end();

// $this->registerJsFile("@web/templates/js/custom.js", ['depends' => 'yii\web\JqueryAsset']);
// $this->registerJsFile("@web/templates/js/form.js", ['depends' => 'yii\web\JqueryAsset']);
// $this->registerJsFile("@web/templates/js/pages/uiTables.js", ['depends' => 'yii\web\JqueryAsset']);

$urlCreate = Url::toRoute(['/kursus/create']);
?>
<div class="coach-index">
    <?php if (User::getType()==2 && User::getStatusSiswa()=='memilih kursus') { ?>
        <?php if (Siswa::checkWaktuPembayaran()) {?>        
            <div class="row">
                <a href="javascript:void(0)" class="widget">
                    <div class="widget-content themed-background-danger text-light-op text-center">
                        Batas Pembayaran Anda Tinggal
                        <strong><?= Siswa::getSisaWaktuPembayaran()?> Hari Lagi</strong>
                    </div>
                </a>
            </div>
            <?= Html::a('Upload Bukti Pembayaran', ['/pembayaran/create'], ['class' => 'btn btn-success']) ?>
        <?php }?>
        <?= Html::a('Ganti Kursus', ['view', 'id' => 1], ['class' => 'btn btn-danger']) ?>
    <?php } ?>
    <?php if (User::getType()==1){?>
        <?=Html::button('<i class="fa fa-plus"></i>', ['value'=>$urlCreate,'class' => 'btn btn-primary btn-flat btn-modal btn-sm']);?>
    <?php } ?>
    <div class="block full">
        <?php
            foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
            }
        ?>
        <div class="block-title">
            <h2>Datatables</h2>
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">ID</th>
                        <th>Nama</th>
                        <th style="width: 50px;">Kuota</th>
                        <th style="width: 250px;">Periode</th>
                        <th style="width: 50px;">Status</th>
                        <th class="text-center" style="width: 80px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataKursus as $key => $value) { 
                        $urlUpdate = Url::toRoute(['/kursus/update', 'id' => $value->id]);
                        $urlViewJadwal = Url::toRoute(['/kursus/view', 'id' => $value->id]);
                    ?>
                    <tr>
                        <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
                        <td><strong><?=strtoupper($value->nama)?></strong></td>
                        <td><?=$value->quota?></td>
                        <td><b><?=Yii::$app->formatter->asDate($value->tanggal_awal, 'dd MMMM Y')?></b> s/d <b> <?=Yii::$app->formatter->asDate($value->tanggal_akhir, 'dd MMMM Y')?></b></td>
                        <td><span class="label label-<?=$value->classLabel?>"><?=$value->statusString?></span></td>
                        <td class="text-center">
                            <?php if (User::getType()==2){ ?>
                                <?=Html::button('<i class="fa fa-eye"></i>', ['value'=>$urlViewJadwal,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal']);?>
                            <?php }else{?>
                                <?php if (User::getType()==1 && $value->status !=0) {?>
                                    <?=Html::button('<i class="fa fa-pencil"></i>', ['value'=>$urlUpdate,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal']);?>
                                    <?= Html::a('<i class="fa fa-times"></i>', ['delete', 'id' => $value->id], [
                                        'class' => 'btn btn-effect-ripple btn-xs btn-danger',
                                         'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],]) ?>
                                <?php } ?>
                                <?= Html::a('<i class="fa fa-eye"></i>', ['view', 'id' => $value->id], [
                                    'class' => 'btn btn-effect-ripple btn-xs btn-success']) ?>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>$(function(){ UiTables.init(); });</script>
