<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kursus */

$this->title = 'Create Kursus';
$this->params['breadcrumbs'][] = ['label' => 'Kursuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kursus-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelJadwal'=>$modelJadwal

    ]) ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal-header').html('<h4>Create Kursus<h4>');
    });
</script>