<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Jadwal;
?>

<div class="siswa-index">
    <div class="block full">
        <div class="block-title">
            <h2>Datatables</h2>
        </div>
        <div class="table-responsive">
            <table id="myTable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataAbsensi as $key => $value) { 
                    ?>
                    <tr>
                        <td><strong><?=strtoupper($value->siswa->namaSiswa)?></strong></td>
                        <td><?= ($value->keterangan == 'h') ? '<span class="label label-success"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-times"></i></span>'?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>    
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
</script>
