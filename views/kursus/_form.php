<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Kursus */
/* @var $form yii\widgets\ActiveForm */
// $this->registerCssFile("@web/web/templates/css/dynamicform.css");
$this->registerJsFile("@web/web/templates/js/dynamicform.js", ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("@web/web/templates/css/plugins.css");

?>

<div class="block">
    <!-- Clickable Wizard Title -->
    <!-- END Clickable Wizard Title -->

    <!-- Clickable Wizard Content -->
    <?php $form = ActiveForm::begin([
        'id' => 'clickable-wizard',
    ]); ?>
        
        <div id="clickable-first" class="step">
            <!-- Step Info -->
            <div class="form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified clickable-steps">
                        <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-first"><i class="fa fa-user"></i> <strong>Pelatihan</strong></a></li>
                        <?php if ($model->isNewRecord) {?>
                        <li><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-pencil-square-o"></i> <strong>Jadwal</strong></a></li>
                    <?php } ?>
                        
                    </ul>
                </div>
            </div>
            <!-- END Step Info -->

            <?= $form->field($model, 'nama')->textInput() ?>

            <?= $form->field($model, 'quota')->textInput() ?>
            <?php if (!$model->isNewRecord) {
                echo $form->field($model, 'status')->widget(Select2::classname(), [
                    'data' => $model->getArrayStatus(),
                    'options' => ['placeholder' => 'Pilih Mata Pelajaran'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);;
            }?>
            
        </div>
        <!-- END First Step -->
        <?php if ($model->isNewRecord) {?>
        <!-- Second Step -->
        <div id="clickable-second" class="step">
            <!-- Step Info -->
            <div class="form-group" style="    height: 68px;
">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified clickable-steps">
                        <li><a href="javascript:void(0)" class="text-muted" data-gotostep="clickable-first"><i class="fa fa-user"></i> <del><strong>Pelatihan</strong></del></a></li>
                        <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-pencil-square-o"></i> <strong>Jadwal</strong></a></li>
                        
                    </ul>
                </div>
            </div>
            <!-- END Step Info -->
            <div data-role="dynamic-fields">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button class="btn btn-danger" data-role="remove">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button>
                            <button class="btn btn-primary" data-role="add">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                
                            <?= $form->field($modelJadwal, 'mapel[]')->textInput() ?>
                            </div>
                        
                            <div class="col-md-6"> 
                               <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-select2">Ruangan</label>
                                        <select id="example-select2" name="ruangan[]" class="select-select2" style="width: 100%;" data-placeholder="Pilih Ruangan">
                                            <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                            <?php foreach ($modelJadwal->arrayRuang as $key => $value) {?>
                                                <option value="<?=$key?>"><?= $value?></option>
                                            <?php }?>
                                        </select>
                                </div>

                            </div>
                            <div class="col-md-6"> 
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-select2">Coach</label>
                                        <select id="example-select2" name="coach[]" class="select-select2" style="width: 100%;" data-placeholder="Pilih Coach">
                                            <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                            <?php foreach ($modelJadwal->arrayCoach as $key => $value) {?>
                                                <option value="<?=$key?>"><?= $value?></option>
                                            <?php }?>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-4"> 
                                <?= $form->field($modelJadwal, 'hari[]')->widget(DatePicker::classname(), [
                                        'options' => ['placeholder' => 'Enter birth date ...'],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'format' => 'yyyy-mm-dd'
                                            ]
                                        ])->label('Tanggal Mulai');
                                ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($modelJadwal, 'waktu_mulai[]')->widget(TimePicker::classname(), [
                                        'options' => [],
                                        'pluginOptions' => [
                                            'showSeconds' => false,
                                            'showMeridian' => false,
                                            'minuteStep' => 1,
                                        ]
                                    ])->label('Jam Mulai');
                                ?>
                            </div>
                            <div class="col-md-4"> 
                                <?= $form->field($modelJadwal, 'waktu_selesai[]')->widget(TimePicker::classname(), [
                                        'options' => [],
                                        'pluginOptions' => [
                                            'showSeconds' => false,
                                            'showMeridian' => false,
                                            'minuteStep' => 1,
                                        ]
                                    ])->label('Jam Selesai');
                                ?>
                            </div>
                        </div>
                        
                    </div>
                 <!-- /div.form-inline -->
            </div>  <!-- /div[data-role="dynamic-fields"] -->

        </div>
        <?php } ?>
        <div class="form-group form-actions">
            <div class="col-md-8 col-md-offset-4">
                <button type="reset" class="btn btn-effect-ripple btn-danger" id="back1">Back</button>
                <button type="submit" class="btn btn-effect-ripple btn-primary" id="next1">Next</button>
            </div>
        </div>
        <!-- END Form Buttons -->
    <?php ActiveForm::end(); ?>
    <!-- END Clickable Wizard Content -->
</div>

<script src="<?=Yii::$app->homeUrl?>web/templates/js/pages/formsWizard.js"></script>
<script>$(function(){ FormsWizard.init(); });</script>
<script type="text/javascript">
    $(".select-select2").select2({
        maximumSelectionLength: 4,
        placeholder: "With Max Selection limit 4",
        allowClear: true
    });

</script>