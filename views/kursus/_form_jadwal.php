
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
/* @var $this yii\web\View */
/* @var $model app\models\Kursus */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile("@web/web/templates/js/pages/formsWizard.js", ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("@web/web/templates/css/custom.css");
?>

<?php $form = ActiveForm::begin([
        'id' => 'clickable-wizard',
    ]); ?>
    <?= $form->field($modelJadwal, 'id_coach')->widget(Select2::classname(), [
        'data' => $modelJadwal->kursus->getArrayCoach(),
        'options' => ['placeholder' => 'Pilih Coach'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($modelJadwal, 'id_ruangan')->widget(Select2::classname(), [
        'data' => $modelJadwal->kursus->getArrayRuang(),
        'options' => ['placeholder' => 'Pilih Ruangan'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($modelJadwal, 'waktu_mulai')->widget(TimePicker::classname(), [
        'options' => ['placeholder' => 'Pilih Ruangan'],
        'pluginOptions' => [
            'showSeconds' => true
        ],
    ]); ?>

    <?= $form->field($modelJadwal, 'waktu_selesai')->widget(TimePicker::classname(), [
        'options' => ['placeholder' => 'Pilih Ruangan'],
        'pluginOptions' => [
            'showSeconds' => true
        ],
    ]); ?>
    <button type="submit" class="btn btn-effect-ripple btn-primary" id="next1">Next</button>
<?php ActiveForm::end(); ?>