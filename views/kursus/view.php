<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\Kursus */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile("@web/web/templates/js/pages/formsWizard.js", ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("@web/web/templates/css/custom.css");
// $this->registerJsFile("@web/web/templates/js/custom.js", ['depends' => 'yii\web\JqueryAsset']);
$this->title = 'Kursus '. $model->nama;
Modal::begin([
        'header'=>'<h4>Update Jadwal</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
    ]);
echo "<div id='content-modal'></div>";
Modal::end();

?>
<?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
    }
?>
<?php if (User::getType()==4 && $model->status == 0){?>
        <?= Html::a('Setujui', ['setujui-kursus', 'idKursus' => $model->id], ['class' => 'btn btn-effect-ripple btn-success'])?>
    <?php }?>
<div class="block">
    <?php $form = ActiveForm::begin([
        'id' => 'clickable-wizard',
    ]); ?>

        <div id="clickable-first" class="step">
            <!-- Step Info -->
            <div class="form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified clickable-steps">
                        <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-first"><i class="fa fa-user"></i> <strong>Kursus</strong></a></li>
                        <li><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-pencil-square-o"></i> <strong>Jadwal</strong></a></li>
                        
                    </ul>
                </div>
            </div>
            <hr>
            <hr>
            <hr>
            <!-- END Step Info -->

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id_mapel',
                    'nama',
                    'tanggal_awal',
                    'tanggal_akhir',
                    'quota',
                    [
                        'attribute'=>'status',
                        'format'=> 'raw',
                        'value'=>function($model){
                            return '<span class="label label-'.$model->classLabel.'">'.$model->statusString.'</span>';
                        }
                    ]
                ],
            ]) ?>
            
        </div>
        <!-- END First Step -->

        <!-- Second Step -->
        <div id="clickable-second" class="step">
            <!-- Step Info -->
            <div class="form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified clickable-steps">
                        <li><a href="javascript:void(0)" class="" data-gotostep="clickable-first"><i class="fa fa-user"></i><strong>Kursus</strong></a></li>
                        <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-pencil-square-o"></i> <strong>Jadwal</strong></a></li>
                        
                    </ul>
                </div>
            </div>
            <hr>
            <hr>
            <hr>
            <!-- END Step Info -->
            <div class="">
                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                    <thead>
                        <tr>
                            
                            <th>Coach</th>
                            <th>Ruangan</th>
                            <th>Hari</th>
                            <th>Mulai</th>
                            <th>Berakhir</th>
                            <?php if ((User::getType()==2 && User::getStatusSiswa()=='peserta') || User::getType()==3) {?>
                                <th>Absensi</th>
                            <?php } ?>
                            <?php if (User::getType()==1 ||User::getType()==3) {?>
                            <th class="text-center" style="width: 75px;"><i class="fa fa-flash"></i></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($modelJadwal as $key => $value) { 
                            $urlUpdate = Url::toRoute(['/kursus/update-jadwal', 'id' => $value->id]);
                            $urlAbsensi = Url::toRoute(['/kursus/view-siswa', 'idKursus' => $model->id, 'idJadwal'=>$value->id]);
                            $urlViewAbsensi = Url::toRoute(['/kursus/view-absensi', 'idJadwal'=>$value->id]);
                        ?>
                        <tr>
                            
                            <td><strong><?=strtoupper($value->coach->user->biodataUsers->nama)?></strong></td>
                            <td><strong><?=strtoupper($value->ruangan->nama)?></strong></td>
                            <td><?=Yii::$app->formatter->asDate($value->hari, 'dd MMMM Y')?></td>
                            <td><?=$value->waktu_mulai?></td>
                            <td><?=$value->waktu_selesai?></td>
                            <?php if ((User::getType()==2 && User::getStatusSiswa()=='peserta') || User::getType()==3) { ?>
                                <td><?php echo $value->absensi; ?></td>
                            <?php } ?>
                            <?php if (User::getType()==1 ||User::getType()==3) {?>
                            <td>
                                <?php if (User::getType()==1 && $value->kursus->status != 0 ){ ?>
                                <?=Html::button('<i class="fa fa-pencil"></i>', ['value'=>$urlUpdate,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal', 'title'=>'Update Kursus']);?>
                                <?php }else if (User::getType()==3) {?>
                                    <?=Html::button('<i class="fa fa-info"></i>', ['value'=>$urlAbsensi,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal', 'title'=>'Absensi']);?>
                                <?php } ?>

                                <?=Html::button('<i class="fa fa-eye"></i>', ['value'=>$urlViewAbsensi,'class' => 'btn btn-effect-ripple btn-xs btn-primary btn-modal', 'title'=>'Rekap Absensi']);?>

                            </td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="form-group">
        </div>
        <!-- END Form Buttons -->
    <?php ActiveForm::end(); ?>
    <!-- END Clickable Wizard Content -->
    <?php if (User::getType()==2 && User::getStatusSiswa()=='mendaftar') { ?>
        <?php if ($model->sisaQuota != 0) {?>
            <?= Html::a('Pilih', ['pilih-kursus', 'idKursus' => $model->id], ['class' => 'btn btn-effect-ripple btn-success']) ?>
        <?php }else{ ?>
            <?= Html::a('Penuh', ['#'], ['class' => 'btn btn-effect-ripple btn-danger']) ?>
        <?php } ?>
    <?php } ?>
</div>

<?php if (User::getType()==4) { ?>
<div class="panel panel-default">
    <div class="panel-heading">Daftar Siswa</div>
    <div class="panel-body">
        <table id="myTable" class="table table-striped table-bordered table-vcenter">
            <thead>
                <tr>
                    <th>Nama</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataSiswaKursus as $key => $value) { 
                ?>
                <tr>
                    <td><strong><?=strtoupper($value->siswa->namaSiswa)?></strong></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php } ?>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="<?=Yii::$app->homeUrl?>web/templates/js/pages/formsWizard.js"></script>
<script>$(function(){ FormsWizard.init(); });</script>
<script>$(function(){ UiTables.init(); });</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
        $('.modal-header').html('<h4>Pilih Kursus<h4>');
    });
</script>