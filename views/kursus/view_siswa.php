<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Jadwal;
?>


<?php $form = ActiveForm::begin([
        'id' => 'clickable-wizard',
        'action'=>['save-absensi']
]); ?>

<?php if (Jadwal::getCheckAbsensi($idJadwal)) { ?>
    <i>Data telah Tersimpan</i>
<?php }else{ ?>

<input type="hidden" name="id_jadwal" value="<?=$idJadwal?>">
<input type="hidden" name="id_kursus" value="<?=$idKursus?>">
<div class="siswa-index">
    <div class="block full">
        <div class="block-title">
            <h2>Datatables</h2>
        </div>
        <div class="table-responsive">
            <table id="myTable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th style="width: 80px;" class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox" checked="true"><span></span></label></th>
                        <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataSiswa as $key => $value) { 
                    ?>
                    <tr>
                        <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox" name="absensi[]" value="<?=$value->siswa->id?>"><span></span></label></td>
                        <td><strong><?=strtoupper($value->siswa->namaSiswa)?></strong></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>    
</div>
<?php ActiveForm::end(); ?>
<?php } ?>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable();
        $('input:checkbox').each(function() {
            $(this).prop('checked', true);
        });

    } );
</script>
