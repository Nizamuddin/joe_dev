<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pembayaran */

$this->title = 'Create Pembayaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pembayaran-form">
    <div class="block">
        <!-- Horizontal Form Title -->
        <div class="block-title">
            <h2>Pembayaran</h2>
        </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
    

</div>		