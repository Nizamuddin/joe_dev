<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pembayaran */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pembayarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="widget">
            <div class="widget-image widget-image-sm">
                <img src="<?=Yii::$app->homeUrl?>templates/img/placeholders/photos/photo1@2x.jpg" alt="image">
                <div class="widget-image-content text-center">
                    <img src="<?=Yii::$app->homeUrl?>/<?=$model->siswa->pathFoto?>" alt="avatar" class="img-circle img-thumbnail img-thumbnail-transparent img-thumbnail-avatar-2x push">
                    <h2 class="widget-heading text-light"><strong><?=$model->siswa->user->biodataUsers->nama?></strong></h2>
                    <h4 class="widget-heading text-light-op"><em><?=$model->siswa->user->email?></em></h4>
                </div>
            </div>
            <div class="widget-content border-bottom">
               <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'nominal',
                        'jenis',
                        'catatan:ntext',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

