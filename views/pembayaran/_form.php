<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\Pembayaran */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
        }
    ?>
        <!-- END Horizontal Form Title -->

        <!-- Horizontal Form Content -->
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'nominal')->textInput() ?>

        <?= $form->field($model, 'jenis')->dropDownList(
                $model->getType()
        ); ?>

        <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'file')->widget(FileInput::classname(), [
                            'options' => [
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
                            ],

                            'language' => 'en',
                            'pluginOptions' => [
                                'browseClass' => 'btn btn-success',
                                'browseLabel' =>  'Unggah',
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false,
                                'required'  => true
                                // 'mainClass' => 'btn-loginregist'
                            ],
                        ])->label('Bukti Transfer')

            ?>
        <?= $form->field($model, 'atas_nama')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <!-- END Horizontal Form Content -->

