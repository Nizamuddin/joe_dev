<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use dmstr\bootstrap\Tabs;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pembayaran';
$this->params['breadcrumbs'][] = $this->title;
// $this->registerJsFile("@web/templates/js/custom.js", ['depends' => 'yii\web\JqueryAsset']);

Modal::begin([
        'header'=>'<h4>Detail Siswa</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
echo "<div id='content-modal'></div>";
Modal::end();
?>

<?php $this->beginBlock('belum'); ?>
<div class="table-responsive">
<table id="example-datatable" class="table table-striped table-bordered table-vcenter">
    <thead>
        <tr>
            <th style="width: 80px;" class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></th>
            <th>Nama</th>
            <th style="width: 80px;">Jenis</th>
            <th>Catatan</th>
            <th style="width: 220px;" class="text-center"><i class="fa fa-flash"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dataBelum as $key => $value) { 
            $urlView = Url::toRoute(['/pembayaran/view', 'id' => $value->id]);
            $tglUpload = str_replace($value->siswa->getBasePath().$value->siswa->user->id.'/'.$value->siswa->user->id.'-', '', $value->bukti_transfer);
            $tglUpload = explode(".", $tglUpload)[0];
            $tglUpload = date('m/d/Y', $tglUpload);
            $urlVerifikasi = Url::toRoute(['/pembayaran/verifikasi', 'id' => $value->id]);
        ?>
        <tr>
            <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
            <td><strong><?=strtoupper($value->siswa->user->biodataUsers->nama)?></strong></td>
            <td><?=strtoupper($value->jenis)?></td>
            <td><?=$value->catatan?></td>
            <td class="text-center">
                <?=Html::button('<i class="fa fa-info-circle"></i>', ['value'=>$urlView,'class' => 'btn btn-primary btn-flat btn-modal btn-sm']);?>
                <?= Html::a('<i class="gi gi-picture"></i>', [$value->bukti_transfer, 'id' => $value->id], ['class' => 'btn btn-default btn-sm', 'data-toggle'=>'lightbox-image', 'title'=>$tglUpload]) ?>
                <?= Html::a('<i class="gi gi-check"></i>', ['verifikasi', 'id' => $value->id], ['class' => 'btn btn-success btn-sm']) ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
</div>
<?php $this->endBlock() ?>

<?php $this->beginBlock('sudah'); ?>
<div class="table-responsive">
<table id="example-datatable" class="table table-striped table-bordered table-vcenter">
    <thead>
        <tr>
            <th style="width: 80px;" class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></th>
            <th>Nama</th>
            <th style="width: 80px;">Jenis</th>
            <th>Catatan</th>
            <th style="width: 120px;" class="text-center"><i class="fa fa-flash"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dataSudah as $key => $value) { 
            $urlView = Url::toRoute(['/pembayaran/view', 'id' => $value->id]);
            $tglUpload = str_replace($value->siswa->getBasePath().$value->siswa->user->id.'/'.$value->siswa->user->id.'-', '', $value->bukti_transfer);
            $tglUpload = explode(".", $tglUpload)[0];
            $tglUpload = date('m/d/Y', $tglUpload);
        ?>
        <tr>
            <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox"><span></span></label></td>
            <td><strong><?=strtoupper($value->siswa->user->biodataUsers->nama)?></strong></td>
            <td><?=strtoupper($value->jenis)?></td>
            <td><?=$value->catatan?></td>
            <td class="text-center">
                <?=Html::button('<i class="fa fa-info-circle"></i>', ['value'=>$urlView,'class' => 'btn btn-primary btn-flat btn-modal btn-sm']);?>
                <?= Html::a('<i class="gi gi-picture"></i>', [$value->bukti_transfer, 'id' => $value->id], ['class' => 'btn btn-default btn-sm', 'data-toggle'=>'lightbox-image', 'title'=>$tglUpload]) ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
</div>
<?php $this->endBlock() ?>


<div class="pembayaran-index">

    <div class="block full">
        <?php
            foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
            }
        ?>
        <div class="block-title">
            <h2>Pembayaran Siswa</h2>
        </div>
        

        <?= Tabs::widget(
        [
            'id' => 'relation-tabs',
            'encodeLabels' => false,
            'items' => [ 
                [
                    'label'   => '<b class="">Belum Verifikasi</b>',
                    'content' => $this->blocks['belum'],
                    'active'  => true,
                ],
                [
                    'label'   => '<b class="">Sudah Verifikasi</b>',
                    'content' => $this->blocks['sudah'],
                    'active'  => false,
                ],
            ]
        ]
    );?>
    </div>

</div>
<script src="<?=Yii::$app->homeUrl?>web/templates/js/pages/compGallery.js"></script>
<script>$(function(){ CompGallery.init(); });</script>