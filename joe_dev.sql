-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2018 at 08:24 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `joe_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi_coach`
--

CREATE TABLE `absensi_coach` (
  `id` int(11) NOT NULL,
  `id_coach` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `absensi_siswa`
--

CREATE TABLE `absensi_siswa` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `keterangan` char(1) DEFAULT NULL COMMENT 'h,i,s,a',
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `biodata_user`
--

CREATE TABLE `biodata_user` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_ktp` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `hp` varchar(15) NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biodata_user`
--

INSERT INTO `biodata_user` (`id`, `id_user`, `nama`, `no_ktp`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `hp`, `foto`) VALUES
(1, 1, '12321', '12312', '123', '2018-10-15', 'laki-laki', 'asdsad', '1232', 'upload/siswa/1/1-1538846049.png'),
(2, 3, 'coach1', '12312', '123', '2018-10-01', 'laki-laki', 'asdasd', '12312', 'upload/coach/3/3-1538847074.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `coach`
--

CREATE TABLE `coach` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status_menikah` varchar(15) NOT NULL,
  `kewarganegaraan` varchar(20) NOT NULL,
  `pendidikan` text NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1 = aktif; 0 = gak aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coach`
--

INSERT INTO `coach` (`id`, `id_user`, `status_menikah`, `kewarganegaraan`, `pendidikan`, `status`) VALUES
(1, 3, 'sudah', 'sadasd', 'asdsad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id` int(11) NOT NULL,
  `id_kursus` int(11) NOT NULL,
  `id_coach` int(11) NOT NULL,
  `waktu_mulai` time NOT NULL,
  `waktu_selesai` time NOT NULL,
  `hari` date NOT NULL,
  `id_ruangan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id`, `id_kursus`, `id_coach`, `waktu_mulai`, `waktu_selesai`, `hari`, `id_ruangan`) VALUES
(1, 1, 1, '12:30:00', '01:30:00', '2018-11-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karir`
--

CREATE TABLE `karir` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `instansi` varchar(200) NOT NULL,
  `posisi` varchar(200) NOT NULL,
  `tahun_masuk` varchar(15) NOT NULL,
  `tahun_keluar` varchar(15) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=siswa ; 2 = coach'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karir`
--

INSERT INTO `karir` (`id`, `id_user`, `instansi`, `posisi`, `tahun_masuk`, `tahun_keluar`, `type`) VALUES
(1, 3, 'asdsa', 'asd', '2018-11-06', '2018-11-06', 2),
(2, 1, 'asdsa', 'asd', '2018-10-07', '2018-10-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kursus`
--

CREATE TABLE `kursus` (
  `id` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `quota` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1 = dibuka; 2 = sedang ; 3 = ditutup'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kursus`
--

INSERT INTO `kursus` (`id`, `id_mapel`, `nama`, `tanggal_awal`, `tanggal_akhir`, `quota`, `status`) VALUES
(1, 1, 'Kursus1', '2018-11-05', '2018-11-09', 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id`, `nama`, `keterangan`) VALUES
(1, 'Mapel1', 'asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `organisasi_coach`
--

CREATE TABLE `organisasi_coach` (
  `id` int(11) NOT NULL,
  `id_coach` int(11) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `tahun_awal` varchar(200) NOT NULL,
  `tahun_akhir` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisasi_coach`
--

INSERT INTO `organisasi_coach` (`id`, `id_coach`, `keterangan`, `tahun_awal`, `tahun_akhir`) VALUES
(1, 1, '123', '2018-11-07', '2018-11-10');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `nominal` double NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `catatan` text NOT NULL,
  `status` int(20) NOT NULL COMMENT '1 = verifikasi ; 0 = belim',
  `bukti_transfer` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `id_siswa`, `nominal`, `jenis`, `catatan`, `status`, `bukti_transfer`) VALUES
(1, 1, 12321, 'tunai', 'dxfcghj', 1, 'upload/siswa/1/1-1538846866.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id`, `nama`, `tempat`, `keterangan`) VALUES
(1, 'HH201', 'PENS', 'asdas');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pendidikan_terakhir` varchar(20) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL COMMENT '''mendaftar, diterima,proses, lulus'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `id_user`, `pendidikan_terakhir`, `jurusan`, `status`) VALUES
(1, 1, '12312', '1231', 'peserta');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_kursus`
--

CREATE TABLE `siswa_kursus` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kursus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_kursus`
--

INSERT INTO `siswa_kursus` (`id`, `id_siswa`, `id_kursus`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=akrif 0=non akrif',
  `type` int(11) NOT NULL COMMENT '1 = admin ; 2 = siswa ; 3 = coach; 4 = SA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `status`, `type`) VALUES
(1, 'siswa@mail.com', '$2y$13$OsxA91uMxYakG0BzsIYQK.EfsCVcJuviJQ3Q20aHyV6Lt0Amf1fYi', 1, 2),
(2, 'nizamuddin.dzaky@gmail.com', '$2y$13$OsxA91uMxYakG0BzsIYQK.EfsCVcJuviJQ3Q20aHyV6Lt0Amf1fYi', 1, 1),
(3, 'coach@mail.com', '$2y$13$XksTIoObwk4O1SwSlW7ire9d76uNlpHIYaXFK4vx9jC9PFt3ti2.6', 1, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi_coach`
--
ALTER TABLE `absensi_coach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_coach` (`id_coach`),
  ADD KEY `id_jadwal` (`id_jadwal`);

--
-- Indexes for table `absensi_siswa`
--
ALTER TABLE `absensi_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jadwal` (`id_jadwal`),
  ADD KEY `absensi_ibfk_2` (`id_siswa`);

--
-- Indexes for table `biodata_user`
--
ALTER TABLE `biodata_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `coach`
--
ALTER TABLE `coach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kursus` (`id_kursus`),
  ADD KEY `id_ruangan` (`id_ruangan`),
  ADD KEY `id_coach` (`id_coach`);

--
-- Indexes for table `karir`
--
ALTER TABLE `karir`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_siswa` (`id_user`);

--
-- Indexes for table `kursus`
--
ALTER TABLE `kursus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kursus_ibfk_2` (`id_mapel`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisasi_coach`
--
ALTER TABLE `organisasi_coach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_coach` (`id_coach`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `siswa_kursus`
--
ALTER TABLE `siswa_kursus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_siswa` (`id_siswa`),
  ADD KEY `id_kursus` (`id_kursus`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi_coach`
--
ALTER TABLE `absensi_coach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `absensi_siswa`
--
ALTER TABLE `absensi_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `biodata_user`
--
ALTER TABLE `biodata_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `coach`
--
ALTER TABLE `coach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `karir`
--
ALTER TABLE `karir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kursus`
--
ALTER TABLE `kursus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `organisasi_coach`
--
ALTER TABLE `organisasi_coach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `siswa_kursus`
--
ALTER TABLE `siswa_kursus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absensi_coach`
--
ALTER TABLE `absensi_coach`
  ADD CONSTRAINT `absensi_coach_ibfk_1` FOREIGN KEY (`id_coach`) REFERENCES `coach` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `absensi_siswa`
--
ALTER TABLE `absensi_siswa`
  ADD CONSTRAINT `absensi_siswa_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `siswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `absensi_siswa_ibfk_2` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id`);

--
-- Constraints for table `biodata_user`
--
ALTER TABLE `biodata_user`
  ADD CONSTRAINT `biodata_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `coach`
--
ALTER TABLE `coach`
  ADD CONSTRAINT `coach_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_kursus`) REFERENCES `kursus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_2` FOREIGN KEY (`id_coach`) REFERENCES `coach` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_ibfk_3` FOREIGN KEY (`id_ruangan`) REFERENCES `ruang` (`id`);

--
-- Constraints for table `karir`
--
ALTER TABLE `karir`
  ADD CONSTRAINT `karir_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kursus`
--
ALTER TABLE `kursus`
  ADD CONSTRAINT `kursus_ibfk_1` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id`);

--
-- Constraints for table `organisasi_coach`
--
ALTER TABLE `organisasi_coach`
  ADD CONSTRAINT `organisasi_coach_ibfk_1` FOREIGN KEY (`id_coach`) REFERENCES `coach` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `siswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswa_kursus`
--
ALTER TABLE `siswa_kursus`
  ADD CONSTRAINT `siswa_kursus_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `siswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `siswa_kursus_ibfk_2` FOREIGN KEY (`id_kursus`) REFERENCES `kursus` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
