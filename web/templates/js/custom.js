$(document).ready(function(){
	$('#user').fadeOut();
	$('#btn-regist').click(function(){
		$('#login').fadeOut();
		$('#user').fadeIn();
	});
	$('#btn-login').click(function(){
		
		$('#user').fadeOut();
		$('#login').fadeIn();
	});

	$('.btn-modal').click(function (){
		
		$('#modal').modal('show')
			.find('#content-modal')
			.load($(this).attr('value'));
	});
});