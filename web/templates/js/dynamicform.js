$(function() {
  // $('.select-select2').select2();
  $(document).ready(function(){
    
  });
    // Remove button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .panel [data-role="remove"]',
        function(e) {
            e.preventDefault();
            $(this).closest('.panel').remove();
            $(".select-select2").select2({
                maximumSelectionLength: 4,
                placeholder: "With Max Selection limit 4",
                allowClear: true
            });
        }
    );
    // Add button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .panel [data-role="add"]',
        function(e) {
            e.preventDefault();
            // $('#jadwal-id_coach').select2('destroy');
            $('.select-select2').select2('destroy');
            var container = $(this).closest('[data-role="dynamic-fields"]');
            new_field_group = container.children().filter('.panel:first-child').clone();
            // container.children('select').select2();
            new_field_group.find('.select-select2').each(function(){
                // $(this).val('');

            });
            container.append(new_field_group);
            // $('#jadwal-id_coach').select2();
            $(".select-select2").select2({
              maximumSelectionLength: 4,
              placeholder: "With Max Selection limit 4",
              allowClear: true
          });
        }
    );
});