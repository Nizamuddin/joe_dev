<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'templates/css/site.css',
        'web/templates/css/plugins.css',
        'web/templates/css/main.css',
        'web/templates/css/themes.css',


    ];
    public $js = [
        'web/templates/js/vendor/modernizr-3.3.1.min.js',
        // 'web/templates/js/vendor/jquery-2.2.4.min.js',
        'web/templates/js/vendor/bootstrap.min.js',
        'web/templates/js/plugins.js',
        'web/templates/js/app.js',
        'web/templates/js/pages/readyDashboard.js',
        'web/templates/js/pages/uiTables.js',
        'web/templates/js/custom.js',
        // 'https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
